package ly.tip.webservice;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Date;

import ly.tip.webservice.restmodels.BankPaymentMethod;
import ly.tip.webservice.restmodels.CardPaymentMethod;
import ly.tip.webservice.restmodels.Comment;
import ly.tip.webservice.restmodels.CoordinatesLocation;
import ly.tip.webservice.restmodels.CustomLocation;
import ly.tip.webservice.restmodels.FoursquareLocation;
import ly.tip.webservice.restmodels.PayPalPaymentMethod;
import ly.tip.webservice.restmodels.PaymentMethod;
import ly.tip.webservice.restmodels.TipAccount;
import ly.tip.webservice.restmodels.TipHistoryEntry;
import ly.tip.webservice.restmodels.TipRequest;
import ly.tip.webservice.restmodels.User;
import ly.tip.webservice.restmodels.UserPhotosStatus;
import ly.tip.webservice.restmodels.UserSettings;
import ly.tip.webservice.restmodels.helpers.DateDeserializer;

public class TiplyGsonFactory {

    public static  Gson tiplyGson() {
        GsonBuilder gsonBuilder = new GsonBuilder();

        // Deserializers
        gsonBuilder.registerTypeAdapter(TipAccount.TipAccountType.class, new TipAccount.TipAccountTypeDeserializer());
        gsonBuilder.registerTypeAdapter(TipHistoryEntry.class, new TipHistoryEntry.TipHistoryEntryDeserializer());
        gsonBuilder.registerTypeAdapter(TipAccount.class, new TipAccount.TipAccountDeserializer());
        gsonBuilder.registerTypeAdapter(PaymentMethod.PaymentType.class, new PaymentMethod.PaymentTypeDeserializer());
        gsonBuilder.registerTypeAdapter(PaymentMethod.PaymentMethodType.class, new PaymentMethod.PaymentMethodTypeDeserializer());
        gsonBuilder.registerTypeAdapter(CardPaymentMethod.CardType.class, new CardPaymentMethod.CardTypeDeserializer());
        gsonBuilder.registerTypeAdapter(BankPaymentMethod.AccountType.class, new BankPaymentMethod.AccountTypeDeserializer());
        gsonBuilder.registerTypeAdapter(PaymentMethod.class, new PaymentMethod.PaymentMethodDeserializer());
        gsonBuilder.registerTypeAdapter(User.UserType.class, new User.UserTypeDeserializer());
        gsonBuilder.registerTypeAdapter(UserPhotosStatus.PhotoStatus.class, new UserPhotosStatus.PhotoStatusDeserializer());
        gsonBuilder.registerTypeAdapter(TipRequest.TipStatus.class, new TipRequest.TipStatusDeserializer());
        gsonBuilder.registerTypeAdapter(CustomLocation.LocationType.class, new CustomLocation.LocationTypeDeserializer());
        gsonBuilder.registerTypeAdapter(CustomLocation.class, new CustomLocation.CustomLocationDeserializer());
        gsonBuilder.registerTypeAdapter(Date.class, new DateDeserializer());

        // Serializers
        gsonBuilder.registerTypeAdapter(User.UserType.class, new User.UserTypeSerializer());
        gsonBuilder.registerTypeAdapter(CardPaymentMethod.class, new CardPaymentMethod.CardPaymentMethodSerializer());
        gsonBuilder.registerTypeAdapter(BankPaymentMethod.class, new BankPaymentMethod.BankPaymentMethodSerializer());
        gsonBuilder.registerTypeAdapter(PayPalPaymentMethod.class, new PayPalPaymentMethod.PayPalPaymentMethodSerializer());
        gsonBuilder.registerTypeAdapter(UserSettings.class, new UserSettings.UserSettingsSerializer());
        gsonBuilder.registerTypeAdapter(User.class, new User.UserSerializer());
        gsonBuilder.registerTypeAdapter(Comment.class, new Comment.CommentSerializer());
        gsonBuilder.registerTypeAdapter(CoordinatesLocation.class, new CoordinatesLocation.CoordinatesLocationSerializer());
        gsonBuilder.registerTypeAdapter(FoursquareLocation.class, new FoursquareLocation.FoursquareLocationSerializer());
        gsonBuilder.registerTypeAdapter(PaymentMethod.class, new PaymentMethod.PaymentMethodSerializer());
        gsonBuilder.registerTypeAdapter(TipRequest.class, new TipRequest.TipRequestSerializer());

        //TODO: Has to be reworked
        gsonBuilder.serializeNulls();

        return gsonBuilder.create();
    }
}
