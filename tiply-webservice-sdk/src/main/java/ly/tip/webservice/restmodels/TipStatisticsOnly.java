package ly.tip.webservice.restmodels;

public class TipStatisticsOnly {
    public double today;
    public double week;
    public double month;
    public double year;
}
