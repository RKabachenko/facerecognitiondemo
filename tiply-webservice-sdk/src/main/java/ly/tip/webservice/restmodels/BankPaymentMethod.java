package ly.tip.webservice.restmodels;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

public class BankPaymentMethod extends PaymentMethod {
    /*
    "routingAba",			// UTF-8 string
    "bankAccountNumber",	// UTF-8 string
    "nameOnAccount",		// UTF-8 string
    "accountType"			// enum 0 - Checking, 1 - Savings
     */

    public enum AccountType {
        CHECKING(0), SAVINGS(1);

        private final int key;

        AccountType(int key) {
            this.key = key;
        }

        public int getKey() {
            return this.key;
        }

        public static AccountType fromKey(int key) {
            for (AccountType type : AccountType.values()) {
                if (type.getKey() == key) {
                    return type;
                }
            }
            return null;
        }
    }

    public static class AccountTypeDeserializer implements JsonDeserializer<AccountType> {

        @Override
        public AccountType deserialize(JsonElement element, Type arg1, JsonDeserializationContext arg2) throws JsonParseException {
            int key = element.getAsInt();
            return AccountType.fromKey(key);
        }
    }

    public static class BankPaymentMethodSerializer implements JsonSerializer<BankPaymentMethod> {

        @Override
        public JsonElement serialize(BankPaymentMethod src, Type typeOfSrc, JsonSerializationContext context) {
            JsonObject jsonObject = new JsonObject();

            jsonObject.addProperty("discriminator", src.type.getStringKey());
            jsonObject.addProperty("methodType", src.methodType.getKey());
            jsonObject.addProperty("default", src.mDefault);
            jsonObject.addProperty("name", src.name);

            jsonObject.addProperty("routingAba", src.routingAba);
            jsonObject.addProperty("bankAccountNumber", src.bankAccountNumber);
            jsonObject.addProperty("nameOnAccount", src.nameOnAccount);
            jsonObject.addProperty("accountType", src.accountType.getKey());

            return jsonObject;
        }
    }

    public String routingAba;
    public String bankAccountNumber;
    public String nameOnAccount;
    public AccountType accountType;

    public BankPaymentMethod() {
        this.type = PaymentType.BANK;
    }
}
