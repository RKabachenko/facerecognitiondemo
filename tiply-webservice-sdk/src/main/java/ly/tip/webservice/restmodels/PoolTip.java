package ly.tip.webservice.restmodels;

public class PoolTip {
//    "tipId" : UUID,
//            "user" : @TipUserModel,
//            "type" : UTF-8, 0 - PLAIN, 1 - ANONYMOUS, is applied for tips and tips' shares,
//            "status" : "UTF-8", (NEW, MATCHED, CANCELED, PAYED, FAILED),
//            "amount" : double,
//            "latitude" : @Latitude,
//            "longitude" : @Longitude,
//            "comment" : string, UTF-8,
//            "rating" : single precision floating point number,
//            "commentedOn" : Date,
//            "version" : integer,
//            "share" :  boolean , in case this tip is a tip share,
//            "shared" : boolean, in case tip is shared,
//            "read" : boolean,
//            "created" : Date
    
    enum PoolTipType {
        PLAIN, ANONYMOUS        
    }
    
    public String tipId;
    public TipUser user;
    public PoolTipType type;
    public String status;
    public double amount;
    public double latitude;
    public double longitude;
    public String comment;
    public float rating;
    public long commentedOn;
    public int version;
    public boolean share;        
    public boolean shared;        
    public boolean read;
    public long created;
}
