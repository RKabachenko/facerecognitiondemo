package ly.tip.webservice.restmodels;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

import ly.tip.webservice.restmodels.helpers.JsonHelper;

public class CustomLocation {
    /*
    "id" : signed int32,
    "userId" : UUID,
    "version" : signed int32,
    "type" : // enum 0 - coordinates, 1 - foursquare
    */

    public enum LocationType {
        COORDINATES(0), FOURSQUARE(1);

        private final int key;

        LocationType(int key) {
            this.key = key;
        }

        public int getKey() {
            return this.key;
        }

        public static LocationType fromKey(int key) {
            for(LocationType type : LocationType.values()) {
                if(type.getKey() == key) {
                    return type;
                }
            }
            return null;
        }
    }

    public static class LocationTypeDeserializer implements JsonDeserializer<LocationType> {

        @Override
        public LocationType deserialize(JsonElement element, Type arg1, JsonDeserializationContext arg2) throws JsonParseException {
            int key = element.getAsInt();
            return LocationType.fromKey(key);
        }

    }

    public int id;
    public String userId;
    public int version;
    public LocationType type;

    public static class CustomLocationDeserializer implements JsonDeserializer<CustomLocation> {
        @Override
        public CustomLocation deserialize(JsonElement element, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            final JsonObject jsonObject = element.getAsJsonObject();

            LocationType type = context.deserialize(jsonObject.get("type"), LocationType.class);

            CustomLocation location;
            if (type == LocationType.COORDINATES) {
                location = new CoordinatesLocation();
                CoordinatesLocation coordinatesLocation = (CoordinatesLocation) location;
                coordinatesLocation.latitude = JsonHelper.getDouble(jsonObject.get("latitude"));
                coordinatesLocation.longitude = JsonHelper.getDouble(jsonObject.get("longitude"));
                coordinatesLocation.name = JsonHelper.getString(jsonObject.get("name"));
                coordinatesLocation.address = JsonHelper.getString(jsonObject.get("address"));
            } else /*if (type == LocationType.FOURSQUARE)*/ {
                location = new FoursquareLocation();
                FoursquareLocation foursquareLocation = (FoursquareLocation) location;
                foursquareLocation.foursquareId = JsonHelper.getString(jsonObject.get("foursquareId"));
            }

            location.id = JsonHelper.getInteger(jsonObject.get("id"));
            location.type = type;
            location.userId = JsonHelper.getString(jsonObject.get("userId"));
            location.version = JsonHelper.getInteger(jsonObject.get("version"));

            return location;
        }

    }
}
