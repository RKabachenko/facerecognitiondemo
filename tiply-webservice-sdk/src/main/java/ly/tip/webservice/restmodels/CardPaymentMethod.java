package ly.tip.webservice.restmodels;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

public class CardPaymentMethod extends PaymentMethod {
    /*--            "cardholdersName" // UTF-8 string
            --            "cardType"    // @CardType
            --            "digits",     // UTF-8 string
            --            "expMonth",   // int
            --            "expYear",    // int
            --            "zipCode",    // UTF-8 string
            --            "cvv"         // UTF-8 string
            */

    public enum CardType {
        VISA("VISA"), AMEX("AMEX"), MASTERCARD("MASTERCARD"), DISCOVER("DISCOVER"), UNKNOWN("UNKNOWN");

        private final String key;

        CardType(String key) {
            this.key = key;
        }

        public String getKey() {
            return this.key;
        }

        public static CardType fromKey(String key) {
            for (CardType type : CardType.values()) {
                if (type.getKey().equals(key)) {
                    return type;
                }
            }
            return null;
        }
    }

    public static class CardTypeDeserializer implements JsonDeserializer<CardType> {

        @Override
        public CardType deserialize(JsonElement element, Type arg1, JsonDeserializationContext arg2) throws JsonParseException {
            String key = element.getAsString();
            return CardType.fromKey(key);
        }

    }

    public static class CardPaymentMethodSerializer implements JsonSerializer<CardPaymentMethod> {

        @Override
        public JsonElement serialize(CardPaymentMethod src, Type typeOfSrc, JsonSerializationContext context) {
            JsonObject jsonObject = new JsonObject();

            jsonObject.addProperty("discriminator", src.type.getStringKey());
            jsonObject.addProperty("methodType", src.methodType.getKey());
            jsonObject.addProperty("default", src.mDefault);
            jsonObject.addProperty("name", src.name);

            jsonObject.addProperty("cardholdersName", src.cardholdersName);
            jsonObject.addProperty("cardType", src.cardType.getKey());
            jsonObject.addProperty("digits", src.digits);
            jsonObject.addProperty("expMonth", String.valueOf(src.expMonth));
            jsonObject.addProperty("expYear", String.valueOf(src.expYear));
            jsonObject.addProperty("zipCode", src.zipCode);
            jsonObject.addProperty("cvv", src.cvv);

            return jsonObject;
        }
    }

    public String cardholdersName;
    public CardType cardType;
    public String digits;
    public Integer expMonth;
    public Integer expYear;
    public String zipCode;
    public String cvv;

    public CardPaymentMethod() {
        this.type = PaymentType.CARD;
        this.methodType = PaymentMethodType.TIP;
    }


    public CardPaymentMethod(Parcel in) {
        super(in);
        cardholdersName = in.readString();
        cardType = CardType.fromKey(in.readString());
        digits = in.readString();
        expMonth = in.readInt();
        expYear = in.readInt();
        zipCode = in.readString();
        cvv = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(cardholdersName);
        dest.writeString(cardType.getKey());
        dest.writeString(digits);
        dest.writeInt(expMonth);
        dest.writeInt(expYear);
        dest.writeString(zipCode);
        dest.writeString(cvv);
    }

    public static final Parcelable.Creator<CardPaymentMethod> CREATOR = new Parcelable.Creator<CardPaymentMethod>() {
        public CardPaymentMethod createFromParcel(Parcel in) {
            return new CardPaymentMethod(in);
        }

        public CardPaymentMethod[] newArray(int size) {
            return new CardPaymentMethod[size];
        }
    };

    public static CardType determineCardType(String cardNumber) {
        CardType type = CardType.UNKNOWN;
        if (cardNumber !=null && cardNumber.trim().length() >= 2) {

            int cardPrefix = Integer.parseInt(cardNumber.substring(0, 2));
            if (cardPrefix >= 40 && cardPrefix <= 49) {
                type = CardType.VISA;
            } else if (cardPrefix == 37 || cardPrefix == 34) {
                type = CardType.AMEX;
            } else if (cardPrefix >= 50 && cardPrefix <= 59) {
                type = CardType.MASTERCARD;
            } else if (cardPrefix == 60 || cardPrefix == 62 || cardPrefix == 64 || cardPrefix == 65) {
                type = CardPaymentMethod.CardType.DISCOVER;
            }
        }
        return type;
    }

}
