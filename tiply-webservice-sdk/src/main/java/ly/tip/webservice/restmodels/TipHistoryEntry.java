package ly.tip.webservice.restmodels;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.annotations.SerializedName;

import java.lang.reflect.Type;

import ly.tip.webservice.restmodels.helpers.JsonHelper;

public class TipHistoryEntry {
/*
  "user" : @TipUserModel,
  "tipId" : UUID,
  "status" : "UTF-8", (NEW, MATHED, CANCELED, PAYED, FAILED) //TODO: typo?
  "tipPhotoId" : UUID(NULL in case anonymous tip)
  "amount" : double,
  "timestamp" : long,
  "hidden" : boolean
*/

    public enum TipHistoryEntryStatus {
        @SerializedName("NEW")
        NEW("NEW"),
        @SerializedName("MATCHED")
        MATCHED("MATCHED"),
        @SerializedName("CANCELED")
        CANCELED("CANCELED"),
        @SerializedName("PAYED")
        PAYED("PAYED"),
        @SerializedName("FAILED")
        FAILED("FAILED");

        private final String key;

        TipHistoryEntryStatus(String key) {
            this.key = key;
        }

        public String getKey() {
            return this.key;
        }

        public static TipHistoryEntryStatus fromKey(String key) {
            for (TipHistoryEntryStatus type : TipHistoryEntryStatus.values()) {
                if (type.getKey().equals(key)) {
                    return type;
                }
            }
            return null;
        }
    }

    public TipHistoryEntry() {
    }

    public String tipId;
    public double amount;
    public long timestamp;
    public boolean hidden;
    public TipUser user;
    public TipPool pool;
    
    public boolean read;
    public boolean share;
    public boolean shared;
    //public boolean Type type; TODO: Not implemented now

    public TipHistoryEntryStatus status;
    public String tipPhotoId;

    //non-serializable properties
    public boolean offline;

    @Override
    public boolean equals(Object o) {
        if (o instanceof TipHistoryEntry) {
            return ((TipHistoryEntry) o).tipId.equals(tipId);
        }
        return super.equals(o);
    }

    public static class TipHistoryEntryDeserializer implements JsonDeserializer<TipHistoryEntry> {
        @Override
        public TipHistoryEntry deserialize(JsonElement element, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            final JsonObject jsonObject = element.getAsJsonObject();
            TipHistoryEntry tipHistoryEntry = new TipHistoryEntry();
            try {
                JsonElement user = jsonObject.get("user");
                TipAccount tipAccount;
                if (!user.isJsonNull()) {
                    tipAccount = context.deserialize(user, TipAccount.class);
                    if (tipAccount.type == TipAccount.TipAccountType.USER) {
                        tipHistoryEntry.user = (TipUser) tipAccount;
                    } else if (tipAccount.type == TipAccount.TipAccountType.POOL) {
                        tipHistoryEntry.pool = (TipPool) tipAccount;
                    }
                }

                tipHistoryEntry.tipId = JsonHelper.getString(jsonObject.get("tipId"));
                tipHistoryEntry.amount = JsonHelper.getDouble(jsonObject.get("amount"));
                tipHistoryEntry.timestamp = JsonHelper.getLong(jsonObject.get("timestamp"));
                tipHistoryEntry.hidden = JsonHelper.getBoolean(jsonObject.get("hidden"));
                tipHistoryEntry.status = TipHistoryEntryStatus.fromKey(JsonHelper.getString(jsonObject.get("status")));

                tipHistoryEntry.read = JsonHelper.getBoolean(jsonObject.get("read"));
                tipHistoryEntry.share = JsonHelper.getBoolean(jsonObject.get("share"));
                tipHistoryEntry.shared = JsonHelper.getBoolean(jsonObject.get("shared"));
                
                JsonElement tipPhotoId = jsonObject.get("tipPhotoId");
                if (!tipPhotoId.isJsonNull()) {
                    tipHistoryEntry.tipPhotoId = JsonHelper.getString(tipPhotoId);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return tipHistoryEntry;
        }
    }
}
