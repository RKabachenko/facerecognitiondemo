package ly.tip.webservice.restmodels;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

public class CoordinatesLocation extends CustomLocation {
    /*
    "latitude" : @Latitude,
  "longitude" : @Longitude,
  "name" : UTF-8 string,
  "address" : UTF-8 string
     */

    public CoordinatesLocation() {
        type = LocationType.COORDINATES;
    }

    public static class CoordinatesLocationSerializer implements JsonSerializer<CoordinatesLocation> {

        @Override
        public JsonElement serialize(CoordinatesLocation src, Type typeOfSrc, JsonSerializationContext context) {
            JsonObject jsonObject = new JsonObject();

            jsonObject.addProperty("type", "coordinates");
            if (src.version > 0) {
                jsonObject.addProperty("version", src.version);
            }

            jsonObject.addProperty("latitude", src.latitude);
            jsonObject.addProperty("longitude", src.longitude);
            jsonObject.addProperty("name", src.name);
            jsonObject.addProperty("address", src.address);

            return jsonObject;
        }
    }

    public double latitude;
    public double longitude;
    public String name;
    public String address;
}
