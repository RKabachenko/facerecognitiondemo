package ly.tip.webservice.restmodels.helpers;

import com.google.gson.JsonElement;

public class JsonHelper {
    public static Integer getInteger(JsonElement object) {
        return object != null ? object.getAsInt() : null;
    }

    public static String getString(JsonElement object) {
        return object != null ? object.getAsString() : null;
    }

    public static Boolean getBoolean(JsonElement object) {
        return object != null ? object.getAsBoolean() : null;
    }

    public static Double getDouble(JsonElement object) {
        return object != null ? object.getAsDouble() : null;
    }

    public static Float getFloat(JsonElement object) {
        return object != null ? object.getAsFloat() : null;
    }

    public static Long getLong(JsonElement object) {
        return object != null ? object.getAsLong() : null;
    }
}
