package ly.tip.webservice.restmodels;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;
import java.util.Date;

public class Comment {
    /*
    "id" : UUID,
  "user" : @TipUserModel,
  "commenter" : @TipUserModel,
  "rating" : single precision floating point number,
  "comment" : UTF-8 text,
  "commentedOn" : timestamp,
  "version" : integer
     */

    public String id;
    public TipUser user;
    public TipUser commenter;
    public Float rating;
    public String comment;
    public Date commentedOn;
    public int version;

    // Internal property
    public boolean commentPrivate;

    public static class CommentSerializer implements JsonSerializer<Comment> {

        @Override
        public JsonElement serialize(Comment src, Type typeOfSrc, JsonSerializationContext context) {
            JsonObject jsonObject = new JsonObject();

            jsonObject.addProperty("rating", src.rating);
            jsonObject.addProperty("comment", src.comment);
            jsonObject.addProperty("commentPrivate", src.commentPrivate);

            return jsonObject;
        }
    }

}
