package ly.tip.webservice.restmodels;

public class Pool {
    public String id;
    public String poolChainId;
    public String name;
    public String address;
    public String photoId;
    public String zipCode;
    public int version;
    public boolean tipReceiptEmailsOn;
}

