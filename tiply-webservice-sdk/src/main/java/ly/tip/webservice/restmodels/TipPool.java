package ly.tip.webservice.restmodels;

import android.os.Parcel;
import android.os.Parcelable;

public class TipPool extends TipAccount {
    public String name;
    public String address;
    public String photoId;

    public TipPool() {/*empty*/}

    public TipPool(Pool pool) {
        id = pool.id;
        type = TipAccountType.POOL;
        name = pool.name;
        address = pool.address;
        photoId = pool.photoId;
        zipCode = pool.zipCode;
        averageRating = 0;
        commentsAmount = 0;
    }

    public TipPool(Parcel in) {
        super(in);
        name = in.readString();
        address = in.readString();
        photoId = in.readString();
    }

    public static final Parcelable.Creator<TipPool> CREATOR = new Parcelable.Creator<TipPool>() {
        public TipPool createFromParcel(Parcel in) {
            return new TipPool(in);
        }

        public TipPool[] newArray(int size) {
            return new TipPool[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(name);
        dest.writeString(address);
        dest.writeString(photoId);
    }
}
