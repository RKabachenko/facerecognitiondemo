package ly.tip.webservice.restmodels;

import android.content.ContentValues;
import android.database.Cursor;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class User implements Serializable {
    public final static int USER_SMILE_PHOTO_ORDER = 0;
    public final static int USER_NONSMILE_PHOTO_ORDER = 1;

    public final static String COLUMN_ID = "_id";
    public final static String COLUMN_USER_ID = "user_id";
    public final static String COLUMN_TERMS_ACCEPTED = "terms_accepted";
    public final static String COLUMN_EMAIL = "email";
    public final static String COLUMN_CURRENT_STEP = "current_step";
    public final static String COLUMN_FACEBOOK_TOKEN = "facebook_token";
    public final static String COLUMN_USER_TYPE = "user_type";

    public final static String COLUMN_PASSWORD = "password";
    public final static String COLUMN_USE_PINCODE = "use_pin_code";
    public final static String COLUMN_PIN_CODE = "pin_code";
    public final static String COLUMN_STAY_SIGNED_IN = "stay_signed_in";
    public final static String COLUMN_DISCOVERABLE_BY_GPS = "discoverable_by_gps";
    public final static String COLUMN_ACCEPT_CARD_TIPS = "accept_card_tips";
    public final static String COLUMN_NONSMILE_PHOTO = "nonsmile_user_photo";
    public final static String COLUMN_SMILE_PHOTO = "smile_user_photo";

    public final static String COLUMN_HAS_PENDING_PHOTO_STATUS = "has_pending_photo_status";
    public final static String COLUMN_HAS_OFFLINE_USER_PHOTOS = "has_offline_user_photos";

    public String id;
    public String firstName;
    public String lastName;
    public String emailAddress;
    public String emailForNotifications;
    public String jobTitle;
    public String paypalAddress;
    public UserType userType;
    public String zipCode;
    public int version;
    public String profilePictureId;
    public boolean termsAccepted;
    public int depositAccountsAmount;
    public int similarPhotosAmount;
    public boolean emailPasswordSet;
    public boolean facebookLinked;
    public String status;
    public List<PaymentMethod> tipMethods;
    public List<PaymentMethod> receiveMethods;
    public String password;
    public String facebookToken;
    public CurrentRegistrationStep currentRegistrationStep;
    public boolean usePinCode = false;
    public boolean acceptCardTips = true;
    public boolean discoverableByGps = false;
    public String pinCode;
    public String nonSmilePhotoId;
    public String smilePhotoId;
    public boolean staySignedIn = true;
    private boolean anonymous;
    public boolean receiveAppNotifications;
    public boolean receiveReceiptEmails;
    public boolean hasPendingPhotoStatus;
    public boolean hasOfflineUserPhotos;

    public static User fromCursor(Cursor cursor) {
        if (cursor != null && cursor.moveToFirst()) {
            User user = new User();
            user.termsAccepted = cursor.getInt(cursor.getColumnIndex(COLUMN_TERMS_ACCEPTED)) == 1;
            user.id = cursor.getString(cursor.getColumnIndex(COLUMN_USER_ID));
            user.emailAddress = cursor.getString(cursor.getColumnIndex(COLUMN_EMAIL));
            user.currentRegistrationStep = CurrentRegistrationStep.fromKey(cursor.getInt(cursor.getColumnIndex(COLUMN_CURRENT_STEP)));
            user.facebookToken = cursor.getString(cursor.getColumnIndex(COLUMN_FACEBOOK_TOKEN));
            user.userType = UserType.fromKey(cursor.getInt(cursor.getColumnIndex(COLUMN_USER_TYPE)));

            user.password = cursor.getString(cursor.getColumnIndex(COLUMN_PASSWORD));
            user.usePinCode = cursor.getInt(cursor.getColumnIndex(COLUMN_USE_PINCODE)) == 1;
            user.acceptCardTips = cursor.getInt(cursor.getColumnIndex(COLUMN_ACCEPT_CARD_TIPS)) == 1;
            user.discoverableByGps = cursor.getInt(cursor.getColumnIndex(COLUMN_DISCOVERABLE_BY_GPS)) == 1;
            user.pinCode = cursor.getString(cursor.getColumnIndex(COLUMN_PIN_CODE));
            user.nonSmilePhotoId = cursor.getString(cursor.getColumnIndex(COLUMN_NONSMILE_PHOTO));
            user.smilePhotoId = cursor.getString(cursor.getColumnIndex(COLUMN_SMILE_PHOTO));
            user.staySignedIn = cursor.getInt(cursor.getColumnIndex(COLUMN_STAY_SIGNED_IN)) == 1;

            user.hasOfflineUserPhotos = cursor.getInt(cursor.getColumnIndex(COLUMN_HAS_OFFLINE_USER_PHOTOS)) == 1;
            user.hasPendingPhotoStatus = cursor.getInt(cursor.getColumnIndex(COLUMN_HAS_PENDING_PHOTO_STATUS)) == 1;

            return user;
        }
        return null;
    }

    public boolean isAnonymous() {
        return anonymous || (emailAddress != null && emailAddress.equals("tiply@tip.ly"));
    }

    public void setAnonymous(boolean anonymous) {
        this.anonymous = anonymous;
    }

    public ContentValues toContentValues() {
        ContentValues values = new ContentValues();

        values.put(COLUMN_TERMS_ACCEPTED, termsAccepted);
        values.put(COLUMN_USER_ID, id);
        values.put(COLUMN_EMAIL, emailAddress);
        values.put(COLUMN_CURRENT_STEP, currentRegistrationStep == null ? 0 : currentRegistrationStep.getKey());
        values.put(COLUMN_FACEBOOK_TOKEN, facebookToken);
        values.put(COLUMN_USER_TYPE, userType.getKey());

        // version 2
        values.put(COLUMN_ACCEPT_CARD_TIPS, acceptCardTips);
        values.put(COLUMN_DISCOVERABLE_BY_GPS, discoverableByGps);
        values.put(COLUMN_NONSMILE_PHOTO, nonSmilePhotoId);
        values.put(COLUMN_SMILE_PHOTO, smilePhotoId);
        values.put(COLUMN_PIN_CODE, pinCode);
        values.put(COLUMN_PASSWORD, password);
        values.put(COLUMN_USE_PINCODE, usePinCode);
        values.put(COLUMN_STAY_SIGNED_IN, staySignedIn);

        values.put(COLUMN_HAS_PENDING_PHOTO_STATUS, hasPendingPhotoStatus);
        values.put(COLUMN_HAS_OFFLINE_USER_PHOTOS, hasOfflineUserPhotos);

        return values;
    }

    public ContentValues toContentValuesForSettings() {
        ContentValues values = new ContentValues();

        values.put(COLUMN_ACCEPT_CARD_TIPS, acceptCardTips ? 1 : 0);
        values.put(COLUMN_DISCOVERABLE_BY_GPS, discoverableByGps ? 1 : 0);
        values.put(COLUMN_NONSMILE_PHOTO, nonSmilePhotoId);
        values.put(COLUMN_SMILE_PHOTO, smilePhotoId);
        values.put(COLUMN_PIN_CODE, pinCode);
        //values.put(COLUMN_PASSWORD, password);
        values.put(COLUMN_USE_PINCODE, usePinCode ? 1 : 0);
        values.put(COLUMN_STAY_SIGNED_IN, staySignedIn ? 1 : 0);

        values.put(COLUMN_HAS_PENDING_PHOTO_STATUS, hasPendingPhotoStatus ? 1 : 0);
        values.put(COLUMN_HAS_OFFLINE_USER_PHOTOS, hasOfflineUserPhotos ? 1 : 0);

        return values;
    }

    public enum UserType {
        TIP_ONLY(0), TIP_RECEIVE_TIPS(1);

        private final int key;

        UserType(int key) {
            this.key = key;
        }

        public static UserType fromKey(int key) {
            for (UserType type : UserType.values()) {
                if (type.getKey() == key) {
                    return type;
                }
            }
            return null;
        }

        public int getKey() {
            return this.key;
        }
    }

    public enum CurrentRegistrationStep {
        FINISHED(0),
        TIP_PREFERENCES(1),
        ADD_CARD(2),
        RECEIVE_TIPS(3),
        FACE_PHOTO(4),
        ADD_FACE(5),
        TERMS_AND_CONDITIONS(6);

        private final int key;

        CurrentRegistrationStep(int key) {
            this.key = key;
        }

        public static CurrentRegistrationStep fromKey(int key) {
            for (CurrentRegistrationStep type : CurrentRegistrationStep.values()) {
                if (type.getKey() == key) {
                    return type;
                }
            }
            return null;
        }

        public int getKey() {
            return this.key;
        }
    }

    public static class UserTypeDeserializer implements JsonDeserializer<UserType> {

        @Override
        public UserType deserialize(JsonElement element, Type arg1, JsonDeserializationContext arg2) throws JsonParseException {
            int key = element.getAsInt();
            return UserType.fromKey(key);
        }

    }

    public static class UserTypeSerializer implements JsonSerializer<UserType> {

        @Override
        public JsonElement serialize(UserType src, Type typeOfSrc, JsonSerializationContext context) {
            return new JsonPrimitive(src.getKey());
        }
    }

    public static class UserSerializer implements JsonSerializer<User> {

        @Override
        public JsonElement serialize(User src, Type typeOfSrc, JsonSerializationContext context) {
            JsonObject jsonObject = new JsonObject();

            jsonObject.addProperty("firstName", src.firstName);
            jsonObject.addProperty("lastName", src.lastName);
            jsonObject.addProperty("emailAddress", src.emailAddress);
            if (src.password != null || src.facebookToken != null) {
                if (src.password != null) {
                    jsonObject.addProperty("password", src.password);
                    jsonObject.addProperty("facebookToken", "");
                } else {
                    jsonObject.addProperty("facebookToken", src.facebookToken);
                }
            }
            jsonObject.addProperty("jobTitle", src.jobTitle);
            jsonObject.addProperty("userType", src.userType.getKey());
            jsonObject.addProperty("termsAccepted", src.termsAccepted);
            jsonObject.addProperty("status", src.status);
            jsonObject.addProperty("zipCode", src.zipCode);
            if (src.version > 0) {
                jsonObject.addProperty("version", src.version);
            }

            return jsonObject;
        }
    }

    public ArrayList<PaymentMethod> creditCards() {
        ArrayList<PaymentMethod> cards = new ArrayList<>();

        if (tipMethods != null) {
            for (PaymentMethod method : tipMethods) {
                if (method instanceof CardPaymentMethod) {
                    cards.add(method);
                }
            }
        }

        return cards;
    }

    public PaymentMethod defaultCreditCard() {
        for (PaymentMethod card : creditCards()) {
            if (card.mDefault) {
                return card;
            }
        }

        return null;
    }

    public ArrayList<PaymentMethod> depositAccounts() {
        ArrayList<PaymentMethod> methods = new ArrayList<>();

        if (receiveMethods != null) {
            for (PaymentMethod method : receiveMethods) {
                if (!(method instanceof CardPaymentMethod)) {
                    methods.add(method);
                }
            }
        }
        return methods;
    }

    public PaymentMethod defaultDepositAccount() {
        for (PaymentMethod method : depositAccounts()) {
            if (method.mDefault) {
                return method;
            }
        }

        return null;
    }

    public void removeCreditCard(PaymentMethod card) {
        if (tipMethods != null && tipMethods.contains(card)) {
            tipMethods.remove(card);
        }
    }

    public void removeDepositAccount(PaymentMethod account) {
        if (receiveMethods != null && receiveMethods.contains(account)) {
            receiveMethods.remove(account);
        }
    }

    public PaymentMethod makeAccountDefault() {
        for (PaymentMethod method : depositAccounts()) {
            if (!method.blocked) {
                method.mDefault = true;
                return method;
            }
        }
        return null;
    }

    public void makeAccountDefault(PaymentMethod account, boolean mDefault) {
        for (PaymentMethod method : depositAccounts()) {
            method.mDefault = method.id.equals(account.id) && mDefault;
        }
    }

    public PaymentMethod makeCreditCardDefault() {        
        // TODO: Remove old default marker 
        for (PaymentMethod method : creditCards()) {
            if (!method.blocked) {
                method.mDefault = true;
                return method;
            }
        }
        return null;
    }

    public void makeCreditCardDefault(PaymentMethod account, boolean mDefault) {
        ArrayList<PaymentMethod> paymentMethods = creditCards();
        for (PaymentMethod method : paymentMethods) {
            method.mDefault = method.id.equals(account.id) && mDefault;
        }
    }

    public int unverifiedAccountsCount() {
        int count = 0;
        for (PaymentMethod method : depositAccounts()) {
            if (!method.verified && method.type == PaymentMethod.PaymentType.BANK && !method.blocked) {
                count++;
            }
        }
        return count;
    }
}
