package ly.tip.webservice.restmodels;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

public class AnonymousTipRequest {
    /*
    "id",				 // @UUID
  "status",			 // @TipStatus
  "amount",			 // signed double
  "latitude",		 // @Latitude
  "longitude",		 // @Longitude
  "version",		 // integer
  "hidden"			 // boolean
     */

    public String id;
    public TipRequest.TipStatus status;
    public double amount;
    public double latitude;
    public double longitude;
    public int version;
    public boolean hidden;

    public PaymentMethod paymentMethod;

    public static class AnonymousTipRequestSerializer implements JsonSerializer<AnonymousTipRequest> {

        @Override
        public JsonElement serialize(AnonymousTipRequest src, Type typeOfSrc, JsonSerializationContext context) {
            JsonObject jsonObject = new JsonObject();

            jsonObject.addProperty("id", src.id);
            jsonObject.addProperty("amount", src.amount);
            jsonObject.addProperty("latitude", src.latitude);
            jsonObject.addProperty("longitude", src.longitude);

            PaymentMethod paymentMethod = src.paymentMethod;
            if (paymentMethod != null) {
                if (paymentMethod.type == PaymentMethod.PaymentType.CARD) {
                    jsonObject.add("paymentMethod", context.serialize(paymentMethod, CardPaymentMethod.class));
                }
                else if (paymentMethod.type == PaymentMethod.PaymentType.BANK) {
                    jsonObject.add("paymentMethod", context.serialize(paymentMethod, BankPaymentMethod.class));
                }
                else {
                    jsonObject.add("paymentMethod", context.serialize(paymentMethod, PayPalPaymentMethod.class));
                }
            }

            return jsonObject;
        }
    }
}
