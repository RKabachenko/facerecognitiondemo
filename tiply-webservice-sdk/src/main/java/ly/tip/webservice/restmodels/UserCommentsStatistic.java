package ly.tip.webservice.restmodels;

public class UserCommentsStatistic {
    /*
    "userId" : UUID,
  "received" : integer,
  "given" : integer,
  "averageRating" : single precision floating point number
     */

    public String userId;
    public int received;
    public int given;
    public float averageRating;
}
