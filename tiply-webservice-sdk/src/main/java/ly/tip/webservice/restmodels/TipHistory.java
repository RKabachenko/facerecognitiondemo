package ly.tip.webservice.restmodels;

public class TipHistory {
    /*
    "tipped" : @TipStatisticsModel
  "received" : @TipStatisticsModel
     */

    public TipStatistics tipped;
    public TipStatistics received;
}
