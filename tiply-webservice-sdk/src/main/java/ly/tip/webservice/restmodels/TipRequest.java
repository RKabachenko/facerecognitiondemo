package ly.tip.webservice.restmodels;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;
import java.util.Date;

public class TipRequest {
    /*
    "id",              // @UUID
  "paymentMethodId", // @UUID
  "recipientId",     // @UUID
  "status",          // @TipStatus
  "amount",          // signed double
  "latitude",        // @Latitude
  "longitude",       // @Longitude
  "comment",         // UTF-8 text
  "rating",          // single precision floating point
  "commentedOn",     // Date
  "version",         // integer
  "hiddenForUser",      // boolean
  "hiddenForRecipient"  // boolean
     */

    public enum TipStatus {
        NEW("NEW"), MATCHED("MATCHED"), CANCELED("CANCELED"), FAILED("FAILED"), PAYED("PAYED");

        private final String key;

        TipStatus(String key) {
            this.key = key;
        }

        public String getKey() {
            return this.key;
        }

        public static TipStatus fromKey(String key) {
            for (TipStatus type : TipStatus.values()) {
                if (type.getKey().equals(key)) {
                    return type;
                }
            }
            return null;
        }
    }

    public static class TipStatusDeserializer implements JsonDeserializer<TipStatus> {

        @Override
        public TipStatus deserialize(JsonElement element, Type arg1, JsonDeserializationContext arg2) throws JsonParseException {
            String key = element.getAsString();
            return TipStatus.fromKey(key);
        }

    }

    public String id;
    public String paymentMethodId;
    public String recipientId;
    public TipStatus status;
    public double amount;
    public double latitude;
    public double longitude;
    public String comment;
    public float rating;
    public Date commentedOn;
    public boolean commentPrivate;
    public int version;
    public boolean hiddenForUser;
    public boolean hiddenForRecipient;

    public static TipRequest tipRequestForCreate(TipRequest request) {
        TipRequest tipRequest = new TipRequest();

        tipRequest.id = request.id;
        tipRequest.recipientId = request.recipientId;
        tipRequest.paymentMethodId = request.paymentMethodId;
        tipRequest.amount = request.amount;
        tipRequest.latitude = request.latitude;
        tipRequest.longitude = request.longitude;
        tipRequest.comment = request.comment;
        tipRequest.rating = request.rating;
        tipRequest.commentPrivate = request.commentPrivate;

        return tipRequest;
    }

    public static class TipRequestSerializer implements JsonSerializer<TipRequest> {

        @Override
        public JsonElement serialize(TipRequest src, Type typeOfSrc, JsonSerializationContext context) {
            JsonObject jsonObject = new JsonObject();

            jsonObject.addProperty("id", src.id);
            jsonObject.addProperty("recipientId", src.recipientId);
            jsonObject.addProperty("paymentMethodId", src.paymentMethodId);
            if (src.version > 0)
                jsonObject.addProperty("version", src.version);
            jsonObject.addProperty("amount", src.amount);
            if (src.latitude != 0)
                jsonObject.addProperty("latitude", src.latitude);
            if (src.longitude != 0)
                jsonObject.addProperty("longitude", src.longitude);

            if (src.comment != null) {
                jsonObject.addProperty("comment", src.comment);
            } else {
                jsonObject.add("comment", JsonNull.INSTANCE);
            }
            if (src.rating > 0) {
                jsonObject.addProperty("rating", src.rating);
            } else {
                jsonObject.add("rating", JsonNull.INSTANCE);
            }

            jsonObject.addProperty("commentPrivate", src.commentPrivate);

            return jsonObject;
        }

    }

    public static TipRequest tipRequestForUpdate(TipRequest request) {
        TipRequest tipRequest = new TipRequest();
        tipRequest.amount = request.amount;
        tipRequest.paymentMethodId = request.paymentMethodId;
        tipRequest.comment = request.comment;
        tipRequest.rating = request.rating;
        tipRequest.version = request.version;
        tipRequest.commentPrivate = request.commentPrivate;

        return tipRequest;
    }


}
