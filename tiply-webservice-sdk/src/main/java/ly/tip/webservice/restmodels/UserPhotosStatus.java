package ly.tip.webservice.restmodels;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

public class UserPhotosStatus {
    public enum PhotoStatus {
        NOT_PROCESSED(0), PROCESSED_TRUE(1), PROCESSED_FALSE(2), PROCESSED_FAILURE(3);

        private final int key;

        PhotoStatus(int key) {
            this.key = key;
        }

        public int getKey() {
            return this.key;
        }

        public static PhotoStatus fromKey(int key) {
            for(PhotoStatus type : PhotoStatus.values()) {
                if(type.getKey() == key) {
                    return type;
                }
            }
            return null;
        }
    }

    public static class PhotoStatusDeserializer implements JsonDeserializer<PhotoStatus> {

        @Override
        public PhotoStatus deserialize(JsonElement element, Type arg1, JsonDeserializationContext arg2) throws JsonParseException {
            int key = element.getAsInt();
            return PhotoStatus.fromKey(key);
        }

    }

    /*
    "id" : UUID,
  "unique" : integer, 0 - NOT_PROCESSED, 1 - PROCESSED_TRUE, 2 - PROCESSED_FALSE, 3 - PROCESSED_FAILURE
  "similar" : integer, 0 - NOT_PROCESSED, 1 - PROCESSED_TRUE, 2 - PROCESSED_FALSE, 3 - PROCESSED_FAILURE
  "order" : UUID
     */

    public String id;
    public PhotoStatus unique;
    public PhotoStatus similar;
    public String order;
}
