package ly.tip.webservice.restmodels;

import android.os.Parcel;
import android.os.Parcelable;

public class TipUser extends TipAccount {
    /*
    "id" : "uuid",
  "firstName" : "UTF-8",
  "lastName" : "UTF-8",
  "jobTitle" : "UTF-8",
  "userType",  // enum 0 - tip-only, 1 - tip-receive-tips
  "profilePictureId" : "UTF-8",
  "status" : "UTF-8"
  "zipCode" : "UTF-8",
  "commentsAmount" : integer
  "averageRating" : single precision floating point number
     */

    public enum Status {
        OK, // 200 Tip confirmed
        FORBIDDEN,  // 403  tip request is in state, other than MATCHED or FAILED;
        // tip request does not have payment method specified;
        // payment method is for receiving;
        // tip recipient missing;
        NOT_FOUND  // payment method override not found;
    }

    public String firstName;
    public String lastName;
    public String jobTitle;
    public User.UserType userType;
    public String profilePictureId;
    public String status;
    public boolean valid;

    public TipUser() {/*empty*/}

    public TipUser(User user) {
        firstName = user.firstName;
        lastName = user.lastName;
        profilePictureId = user.profilePictureId;
    }

    public TipUser(Parcel in) {
        super(in);
        firstName = in.readString();
        lastName = in.readString();
        jobTitle = in.readString();
        userType = User.UserType.valueOf(in.readString());
        profilePictureId = in.readString();
        status = in.readString();
        valid = in.readInt() != 0;

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(firstName);
        dest.writeString(lastName);
        dest.writeString(jobTitle);
        dest.writeString(userType.name());
        dest.writeString(profilePictureId);
        dest.writeString(status);
        dest.writeByte((byte) (valid ? 1 : 0));

    }

    public static final Parcelable.Creator<TipUser> CREATOR = new Parcelable.Creator<TipUser>() {
        public TipUser createFromParcel(Parcel in) {
            return new TipUser(in);
        }

        public TipUser[] newArray(int size) {
            return new TipUser[size];
        }
    };

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof TipUser && hashCode() == o.hashCode();
    }
}
