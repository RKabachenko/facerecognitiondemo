package ly.tip.webservice.restmodels;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.List;

import ly.tip.webservice.restmodels.helpers.JsonHelper;

public class PaymentMethod implements Parcelable, Serializable{

    /*
        --            "id",         // UUID string (128-bit base16 formatted string: 01234567-89ab-cdef-0123-4567-89abcdef0123)
        --            "type",       // enum 0 - card, 1 - paypal, 2-bank
        --            "methodType"  // enum 0 - tip, 1 - receive
        --            "default"     // boolean
        --            "name",       // UTF-8 string, name of payment method
        --            "version"     // int
        */
    public enum VerifyStatus {
        ERROR,
        VERIFIED,
        BLOCKED
    }

    public static class Holder {
        private List<PaymentMethod> methods;
        private String lastInsertedId;

        public Holder(List<PaymentMethod> methods, String lastInsertedId) {
            this.methods = methods;
            this.lastInsertedId = lastInsertedId;
        }

        public List<PaymentMethod> getMethods() {
            return methods;
        }

        public String getLastInsertedId() {
            return lastInsertedId;
        }
    }

    public enum PaymentType {
        CARD(0), PAYPAL(1), BANK(2);

        private final int key;

        PaymentType(int key) {
            this.key = key;
        }

        public int getKey() {
            return this.key;
        }

        public String getStringKey() {
            switch (key) {
                case 0:
                    return "card";
                case 1:
                    return "paypal";
                case 2:
                    return "bank_account";
            }
            return null;
        }

        public static PaymentType fromKey(int key) {
            for (PaymentType type : PaymentType.values()) {
                if (type.getKey() == key) {
                    return type;
                }
            }
            return null;
        }
    }

    public static class PaymentTypeDeserializer implements JsonDeserializer<PaymentType> {

        @Override
        public PaymentType deserialize(JsonElement element, Type arg1, JsonDeserializationContext arg2) throws JsonParseException {
            int key = element.getAsInt();
            return PaymentType.fromKey(key);
        }

    }

    public enum PaymentMethodType {
        TIP(0), RECEIVE(1);

        private final int key;

        PaymentMethodType(int key) {
            this.key = key;
        }

        public int getKey() {
            return this.key;
        }

        public static PaymentMethodType fromKey(int key) {
            for (PaymentMethodType type : PaymentMethodType.values()) {
                if (type.getKey() == key) {
                    return type;
                }
            }
            return null;
        }
    }

    public static class PaymentMethodTypeDeserializer implements JsonDeserializer<PaymentMethodType> {

        @Override
        public PaymentMethodType deserialize(JsonElement element, Type arg1, JsonDeserializationContext arg2) throws JsonParseException {
            int key = element.getAsInt();
            return PaymentMethodType.fromKey(key);
        }

    }

    public String id;
    public PaymentType type;
    public PaymentMethodType methodType;
    @SerializedName("default")
    public boolean mDefault;
    public String name;
    public Integer version;
    public boolean verified;
    public boolean blocked;

    public PaymentMethod() {
    }

    public PaymentMethod(Parcel in) {
        id = in.readString();
        type = PaymentType.fromKey(in.readInt());
        methodType = PaymentMethodType.fromKey(in.readInt());
        mDefault = in.readByte() != 0;
        name = in.readString();
        version = in.readInt();
        verified = in.readByte() != 0;
        blocked = in.readByte() != 0;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeInt(type.getKey());
        dest.writeInt(methodType.getKey());
        dest.writeByte((byte) (mDefault ? 1 : 0));
        dest.writeString(name);
        dest.writeInt(version == null ? 0 : version);
        dest.writeByte((byte) (verified ? 1 : 0));
        dest.writeByte((byte) (blocked ? 1 : 0));
    }

    public static final Parcelable.Creator<PaymentMethod> CREATOR = new Parcelable.Creator<PaymentMethod>() {
        public PaymentMethod createFromParcel(Parcel in) {
            return new PaymentMethod(in);
        }

        public PaymentMethod[] newArray(int size) {
            return new PaymentMethod[size];
        }
    };

    public static class PaymentMethodDeserializer implements JsonDeserializer<PaymentMethod> {
        @Override
        public PaymentMethod deserialize(JsonElement element, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            final JsonObject jsonObject = element.getAsJsonObject();

            PaymentType type = context.deserialize(jsonObject.get("type"), PaymentType.class);

            PaymentMethod method;
            if (type == PaymentType.CARD) {
                method = new CardPaymentMethod();
                CardPaymentMethod cardPaymentMethod = (CardPaymentMethod) method;
                cardPaymentMethod.cardholdersName = JsonHelper.getString(jsonObject.get("cardholdersName"));
                cardPaymentMethod.cardType = context.deserialize(jsonObject.get("cardType"), CardPaymentMethod.CardType.class);
                cardPaymentMethod.digits = JsonHelper.getString(jsonObject.get("digits"));
                cardPaymentMethod.expMonth = JsonHelper.getInteger(jsonObject.get("expMonth"));
                cardPaymentMethod.expYear = JsonHelper.getInteger(jsonObject.get("expYear"));
                cardPaymentMethod.zipCode = JsonHelper.getString(jsonObject.get("zipCode"));
                cardPaymentMethod.cvv = JsonHelper.getString(jsonObject.get("cvv"));
            } else if (type == PaymentType.PAYPAL) {
                method = new PayPalPaymentMethod();
                PayPalPaymentMethod payPalPaymentMethod = (PayPalPaymentMethod) method;
                payPalPaymentMethod.emailAddress = JsonHelper.getString(jsonObject.get("emailAddress"));
            } else {
                method = new BankPaymentMethod();
                BankPaymentMethod bankPaymentMethod = (BankPaymentMethod) method;
                bankPaymentMethod.routingAba = JsonHelper.getString(jsonObject.get("routingAba"));
                bankPaymentMethod.bankAccountNumber = JsonHelper.getString(jsonObject.get("bankAccountNumber"));
                bankPaymentMethod.nameOnAccount = JsonHelper.getString(jsonObject.get("nameOnAccount"));
                bankPaymentMethod.accountType = context.deserialize(jsonObject.get("accountType"), BankPaymentMethod.AccountType.class);
            }

            method.id = JsonHelper.getString(jsonObject.get("id"));
            method.type = type;
            method.methodType = context.deserialize(jsonObject.get("methodType"), PaymentMethodType.class);
            method.mDefault = JsonHelper.getBoolean(jsonObject.get("default"));
            method.name = JsonHelper.getString(jsonObject.get("name"));
            method.version = JsonHelper.getInteger(jsonObject.get("version"));
            method.verified = JsonHelper.getBoolean(jsonObject.get("verified"));
            method.blocked = JsonHelper.getBoolean(jsonObject.get("blocked"));

            return method;
        }

    }

    public static PaymentMethod methodForUpdate(PaymentMethod method) { // Create payment method for updating on server
        PaymentMethod paymentMethod = new PaymentMethod();

        paymentMethod.mDefault = method.mDefault;
        paymentMethod.name = method.name;
        paymentMethod.type = method.type;
        paymentMethod.methodType = method.methodType;
        paymentMethod.version = method.version;

        return paymentMethod;
    }

    public static class PaymentMethodSerializer implements JsonSerializer<PaymentMethod> {

        @Override
        public JsonElement serialize(PaymentMethod src, Type typeOfSrc, JsonSerializationContext context) {
            JsonObject jsonObject = new JsonObject();

            jsonObject.addProperty("default", src.mDefault);
            jsonObject.addProperty("name", src.name);
            jsonObject.addProperty("methodType", src.methodType.getKey());
            jsonObject.addProperty("type", src.type.getStringKey());
            jsonObject.addProperty("version", src.version);

            return jsonObject;
        }
    }


    @Override
    public String toString() {
        return this.name;
    }
}
