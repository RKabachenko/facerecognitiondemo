package ly.tip.webservice.restmodels;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

import ly.tip.webservice.restmodels.helpers.JsonHelper;

public class TipAccount implements Parcelable {

    public String id;
    public TipAccountType type;
    public String zipCode;
    public Integer commentsAmount;
    public float averageRating;

    // non-serializable properties
    public TipAccountMarkerType tipAccountMarkerType;

    public enum TipAccountMarkerType {
        POOL, NEARBY, RECENT, REGISTERED
    }

    public TipAccount() {/*empty*/}

    public TipAccount(Parcel in) {
        id = in.readString();
        type = TipAccountType.fromKey(in.readInt());
        zipCode = in.readString();
        commentsAmount = in.readInt();
        averageRating = in.readFloat();
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeInt(type != null ? type.getKey() : -1);
        dest.writeString(zipCode);
        dest.writeInt(commentsAmount == null ? 0 : commentsAmount);
        dest.writeFloat(averageRating);
    }

    public static final Parcelable.Creator<TipAccount> CREATOR = new Parcelable.Creator<TipAccount>() {
        public TipAccount createFromParcel(Parcel in) {
            return new TipAccount(in);
        }

        public TipAccount[] newArray(int size) {
            return new TipAccount[size];
        }
    };

    public enum TipAccountType {
        USER(0), POOL(1);

        private final int key;

        TipAccountType(int key) {
            this.key = key;
        }

        public int getKey() {
            return this.key;
        }

        public static TipAccountType fromKey(int key) {
            for (TipAccountType type : TipAccountType.values()) {
                if (type.getKey() == key) {
                    return type;
                }
            }
            return null;
        }
    }

    public static class TipAccountTypeDeserializer implements JsonDeserializer<TipAccountType> {

        @Override
        public TipAccountType deserialize(JsonElement element, Type arg1, JsonDeserializationContext arg2) throws JsonParseException {
            int key = element.getAsInt();
            return TipAccountType.fromKey(key);
        }

    }

    public static class TipAccountDeserializer implements JsonDeserializer<TipAccount> {
        @Override
        public TipAccount deserialize(JsonElement element, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            final JsonObject jsonObject = element.getAsJsonObject();

            TipAccountType type = context.deserialize(jsonObject.get("type"), TipAccountType.class);

            TipAccount tipAccount = new TipAccount();

            try {
                if (type == TipAccountType.USER) {
                    TipUser tipUser = new TipUser();
                    tipUser.firstName = JsonHelper.getString(jsonObject.get("firstName"));
                    tipUser.lastName = JsonHelper.getString(jsonObject.get("lastName"));
                    JsonElement jobTitle = jsonObject.get("jobTitle");
                    if (jobTitle.isJsonNull()) {
                        tipUser.jobTitle = null;
                    } else {
                        tipUser.jobTitle = JsonHelper.getString(jobTitle);
                    }
                    tipUser.userType = context.deserialize(jsonObject.get("userType"), User.UserType.class);
                    tipUser.profilePictureId = JsonHelper.getString(jsonObject.get("profilePictureId"));
                    tipUser.status = JsonHelper.getString(jsonObject.get("status"));
                    tipUser.valid = JsonHelper.getBoolean(jsonObject.get("valid"));
                    tipAccount = tipUser;
                } else if (type == TipAccountType.POOL) {
                    TipPool tipPool = new TipPool();
                    JsonElement address = jsonObject.get("address");
                    if (address.isJsonNull()) {
                        tipPool.address = null;
                    } else {
                        tipPool.address = JsonHelper.getString(address);
                    }
                    JsonElement name = jsonObject.get("name");
                    if (name.isJsonNull()) {
                        tipPool.name = null;
                    } else {
                        tipPool.name = JsonHelper.getString(name);
                    }
                    JsonElement photoId = jsonObject.get("photoId");
                    if (photoId.isJsonNull()) {
                        tipPool.photoId = null;
                    } else {
                        tipPool.photoId = JsonHelper.getString(photoId);
                    }
                    tipAccount = tipPool;
                }

                tipAccount.id = JsonHelper.getString(jsonObject.get("id"));
                tipAccount.type = type;
                JsonElement zipCode = jsonObject.get("zipCode");
                tipAccount.zipCode = !zipCode.isJsonNull() ? JsonHelper.getString(zipCode) : null;
                tipAccount.commentsAmount = JsonHelper.getInteger(jsonObject.get("commentsAmount"));
                tipAccount.averageRating = JsonHelper.getFloat(jsonObject.get("averageRating"));
            } catch (Exception e) {
                e.printStackTrace();
            }
            return tipAccount;
        }
    }
}
