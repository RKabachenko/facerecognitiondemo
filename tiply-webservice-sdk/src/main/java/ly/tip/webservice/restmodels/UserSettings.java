package ly.tip.webservice.restmodels;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

public class UserSettings {
    /*
    "userId" : UUID,
    "emailNotificationsOn" : boolean,
    "pushNotificationsOn" : boolean,
    "version" : integer
     */

    public String userId;
    public boolean tipReceiptEmailsOn;
    public boolean tipReceiptApnsOn;
    public int version;

    public static class UserSettingsSerializer implements JsonSerializer<UserSettings> {

        @Override
        public JsonElement serialize(UserSettings src, Type typeOfSrc, JsonSerializationContext context) {
            JsonObject jsonObject = new JsonObject();

            jsonObject.addProperty("tipReceiptEmailsOn", src.tipReceiptEmailsOn);
            jsonObject.addProperty("tipReceiptApnsOn", src.tipReceiptApnsOn);
            jsonObject.addProperty("version", src.version);

            return jsonObject;
        }
    }
}
