package ly.tip.webservice.restmodels;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

public class PayPalPaymentMethod extends PaymentMethod {
   /* "emailAddress"     // UTF-8 string */

    public String emailAddress;
    public String firstName;
    public String lastName;

    public PayPalPaymentMethod() {
        this.type = PaymentType.PAYPAL;
    }

    public static class PayPalPaymentMethodSerializer implements JsonSerializer<PayPalPaymentMethod> {

        @Override
        public JsonElement serialize(PayPalPaymentMethod src, Type typeOfSrc, JsonSerializationContext context) {
            JsonObject jsonObject = new JsonObject();

            jsonObject.addProperty("discriminator", src.type.getStringKey());
            jsonObject.addProperty("methodType", src.methodType.getKey());
            jsonObject.addProperty("default", src.mDefault);
            jsonObject.addProperty("name", "PayPal");
            jsonObject.addProperty("emailAddress", src.emailAddress);

            if (src.firstName != null && !src.firstName.isEmpty()) {
                jsonObject.addProperty("firstname", src.firstName);
            }

            if (src.lastName != null && !src.lastName.isEmpty()) {
                jsonObject.addProperty("lastname", src.lastName);
            }

            return jsonObject;
        }
    }
}
