package ly.tip.webservice.restmodels;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

public class FoursquareLocation extends CustomLocation{
    /*
    "foursquareId" : UTF-8 string
     */

    public FoursquareLocation() {
        this.type = LocationType.FOURSQUARE;
    }

    public static class FoursquareLocationSerializer implements JsonSerializer<FoursquareLocation> {

        @Override
        public JsonElement serialize(FoursquareLocation src, Type typeOfSrc, JsonSerializationContext context) {
            JsonObject jsonObject = new JsonObject();

            jsonObject.addProperty("type", "foursquare");
            if (src.version > 0) {
                jsonObject.addProperty("version", src.version);
            }

            jsonObject.addProperty("foursquareId", src.foursquareId);

            return jsonObject;
        }
    }

    public String foursquareId;
}
