package ly.tip.webservice.restmodels;

import java.util.List;

public class TipStatistics extends TipStatisticsOnly{
    /*
    "today" : double,
  "week" : double,
  "month" : double,
  "year" : double,
  "recent" : [@TipHistoryEntryModel] // array of @TipHistoryEntryModel
     */

    public List<TipHistoryEntry> recent;
}
