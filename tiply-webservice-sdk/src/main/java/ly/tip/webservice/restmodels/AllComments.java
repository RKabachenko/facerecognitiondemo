package ly.tip.webservice.restmodels;

import java.util.List;

public class AllComments {
    /*
    "given" : [ @CommentModel ],
  "received" : [ @CommentModel ]
     */

    public List<Comment> given;
    public List<Comment> received;
}
