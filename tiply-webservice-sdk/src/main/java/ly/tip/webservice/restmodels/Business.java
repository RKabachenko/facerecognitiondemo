package ly.tip.webservice.restmodels;

import java.util.List;

public class Business {

    public String foursquareId;
    public String id;
    public float ratingAverage;
    public float ratingCount;
    public String title;
    public String address;
    public float longitude;
    public float latitude;
    public String category;
    public float distance;
    public List<TipUser> tipReceivers;
}
