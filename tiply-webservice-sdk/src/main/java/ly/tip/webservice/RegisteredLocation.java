package ly.tip.webservice;

import android.location.Location;

import ly.tip.webservice.restmodels.CoordinatesLocation;
import ly.tip.webservice.restmodels.FoursquareLocation;

public class RegisteredLocation {

    private final static double MIN_DISTANCE = 50.0f;
    
    private int id;
    
    private String name;
    private String description;
    private String foursquareId;

    private double latitude;
    private double longitude;

    private CoordinatesLocation coordinatesLocation;
    private FoursquareLocation foursquareLocation;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFoursquareId() {
        return foursquareId;
    }

    public void setFoursquareId(String foursquareId) {
        this.foursquareId = foursquareId;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public RegisteredLocation(CoordinatesLocation coordinateLocation) {
        this.coordinatesLocation = coordinateLocation;

        this.id = coordinateLocation.id;
        this.description = "Custom Location";
        this.name = coordinateLocation.name;
        this.foursquareId = null;
        this.latitude = coordinateLocation.latitude;
        this.longitude = coordinateLocation.longitude;
    }

    public boolean isNearTo(Location location) {
        Location loc = new Location("Location");
        loc.setLatitude(this.latitude);
        loc.setLongitude(this.longitude);
        double distance = location.distanceTo(loc);
        return distance < MIN_DISTANCE;
    }

    public int getLocationId() {
        return id;
    }
    
    public int getFoursquareLocationId() {
        return foursquareLocation != null ? foursquareLocation.id : 0;
    }
    
    public boolean isFoursquareBusiness() {
        return foursquareId != null && !foursquareId.isEmpty() && foursquareLocation != null;
    }

    public void setFoursquareLocation(FoursquareLocation foursquareLocation) {
        this.foursquareLocation = foursquareLocation;
        setFoursquareId(foursquareLocation.foursquareId);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RegisteredLocation that = (RegisteredLocation) o;

        return !(name != null ? !name.equals(that.getName()) : that.getName() != null);
    }

    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }
}
