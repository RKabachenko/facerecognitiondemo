package ly.tip.webservice;

import android.location.Location;

import java.util.ArrayList;
import java.util.List;

import ly.tip.webservice.restmodels.CoordinatesLocation;
import ly.tip.webservice.restmodels.CustomLocation;
import ly.tip.webservice.restmodels.FoursquareLocation;

public class RegisteredLocationsHelper {
    
    public static List<RegisteredLocation> registeredLocations(List<CustomLocation> locations) {
        List<RegisteredLocation> list = new ArrayList<>();
        
        List<CoordinatesLocation> coordsList = new ArrayList<>();
        List<FoursquareLocation> foursquareList = new ArrayList<>();
        
        for (CustomLocation location : locations) {
            if (location.type == CustomLocation.LocationType.COORDINATES) {
                coordsList.add((CoordinatesLocation) location);
            }
            else {
                foursquareList.add((FoursquareLocation) location);
            }
        }
        
        for (CoordinatesLocation location : coordsList) {
            RegisteredLocation registeredLocation = new RegisteredLocation(location);
            
            for (FoursquareLocation foursquareLocation : foursquareList) {
                if (registeredLocation.getName().equals(foursquareLocation.foursquareId)) {
                    registeredLocation.setFoursquareLocation(foursquareLocation);
                    break;
                }
            }
            
            list.add(registeredLocation);
        }
        
        return list;
    }

    public static List<RegisteredLocation> nearbyLocations(List<RegisteredLocation> locations, Location location) {
        List<RegisteredLocation> nearby = new ArrayList<>();
        for (RegisteredLocation registeredLocation : locations) {
            if (!registeredLocation.isFoursquareBusiness() && registeredLocation.isNearTo(location)) {
                nearby.add(registeredLocation);
            }
        }
        return nearby;
    }
    
    public static boolean hasAlreadyRegisteredLocation(List<RegisteredLocation> locations, String name) {
        for (RegisteredLocation registeredLocation : locations) {
            if (name.equals(registeredLocation.getName())) {
                return true;
            }
        }
        return false;
    }

    public static boolean hasAlreadyRegisteredVenue(List<RegisteredLocation> locations, String venueId ) {
        for (RegisteredLocation registeredLocation : locations) {
            if (registeredLocation.isFoursquareBusiness() && registeredLocation.getFoursquareId().equals(venueId)) {
                return true;
            }
        }
        return false;
    }
}
