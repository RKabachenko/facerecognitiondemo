package ly.tip.webservice;

import android.net.Uri;

import com.google.gson.Gson;
import com.squareup.okhttp.OkHttpClient;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.android.AndroidLog;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;

public class TiplyRESTServiceFactory {

    private final static Integer CONNECTION_TIMEOUT = 15 * 1000;
    private final static Integer READ_TIMEOUT = 20 * 1000;
    private static Gson gson;

    public static Gson getGson() {
        return gson;
    }

    public static TiplyRESTService tiplyRESTService(RequestInterceptor requestInterceptor) {
        gson = TiplyGsonFactory.tiplyGson();

        TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return null;
            }

            public void checkClientTrusted(X509Certificate[] certs, String authType) {
            }

            public void checkServerTrusted(X509Certificate[] certs, String authType) {
            }
        }
        };

        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(CONNECTION_TIMEOUT, TimeUnit.MILLISECONDS);
        client.setReadTimeout(READ_TIMEOUT, TimeUnit.MILLISECONDS);

        // Install the all-trusting trust manager
        SSLContext sc;
        try {
            sc = SSLContext.getInstance("SSL");
            try {
                sc.init(null, trustAllCerts, new java.security.SecureRandom());
                client.setSslSocketFactory(sc.getSocketFactory());
                client.setHostnameVerifier(new HostnameVerifier() {
                    @Override
                    public boolean verify(String s, SSLSession sslSession) {
                        return true;
                    }
                });
            } catch (KeyManagementException e) {
                e.printStackTrace();
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        RestAdapter adapter = new RestAdapter.Builder()
//                .setExecutors(Executors.newSingleThreadExecutor(), Executors.newCachedThreadPool())
                .setClient(new OkClient())
                .setRequestInterceptor(requestInterceptor)
                .setConverter(new GsonConverter(gson))
                .setEndpoint(SERVICE_DEVELOPMENT.URL)
                .setLogLevel(RestAdapter.LogLevel.FULL).setLog(new AndroidLog("RETROFIT_NETWORK"))
                .build();

        return adapter.create(TiplyRESTService.class);
    }

    // Internal Server
    public static class SERVICE_DEVELOPMENT {
        private final static String SERVICE_PROTOCOL = "http";
        private final static String SERVICE_URL = "172.20.1.35:8080";
//        private final static String SERVICE_URL = "195.225.144.189:8080";
        private final static String SERVICE_API_POINT = "web-services";
        private final static String URL = new Uri.Builder().scheme(SERVICE_PROTOCOL).encodedAuthority(SERVICE_URL).appendEncodedPath(SERVICE_API_POINT).build().toString();
    }

    // Stage Server
    public static class SERVICE_STAGE {
        private final static String SERVICE_PROTOCOL = "http";
        private final static String SERVICE_URL = "54.68.183.127:8080";
        private final static String SERVICE_API_POINT = "web-services";
        private final static String URL = new Uri.Builder().scheme(SERVICE_PROTOCOL).encodedAuthority(SERVICE_URL).appendEncodedPath(SERVICE_API_POINT).build().toString();
    }

    // Production Server
    public static class SERVICE_PRODUCTION {
        private final static String SERVICE_PROTOCOL = "https";
        private final static String SERVICE_URL = "api.tip.ly";
        private final static String SERVICE_API_POINT = "web-services";
        private final static String URL = new Uri.Builder().scheme(SERVICE_PROTOCOL).encodedAuthority(SERVICE_URL).appendEncodedPath(SERVICE_API_POINT).build().toString();
    }
}
