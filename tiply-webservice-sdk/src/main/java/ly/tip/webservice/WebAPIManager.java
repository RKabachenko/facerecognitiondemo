package ly.tip.webservice;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.JsonObject;
import com.squareup.okhttp.Call;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.List;

import ly.tip.webservice.restmodels.AllComments;
import ly.tip.webservice.restmodels.AnonymousTipRequest;
import ly.tip.webservice.restmodels.Business;
import ly.tip.webservice.restmodels.Comment;
import ly.tip.webservice.restmodels.CustomLocation;
import ly.tip.webservice.restmodels.PaymentMethod;
import ly.tip.webservice.restmodels.Pool;
import ly.tip.webservice.restmodels.TipHistory;
import ly.tip.webservice.restmodels.TipHistoryEntry;
import ly.tip.webservice.restmodels.TipPool;
import ly.tip.webservice.restmodels.TipRequest;
import ly.tip.webservice.restmodels.TipStatisticsOnly;
import ly.tip.webservice.restmodels.TipUser;
import ly.tip.webservice.restmodels.UpdateUserTimeZone;
import ly.tip.webservice.restmodels.User;
import ly.tip.webservice.restmodels.UserCommentsStatistic;
import ly.tip.webservice.restmodels.UserPhotosStatus;
import ly.tip.webservice.restmodels.UserPhotosStatuses;
import ly.tip.webservice.restmodels.UserSettings;
import retrofit.Callback;
import retrofit.RequestInterceptor;
import retrofit.RetrofitError;
import retrofit.client.Header;
import retrofit.client.Response;
import retrofit.mime.MultipartTypedOutput;
import retrofit.mime.TypedOutput;

public class WebAPIManager {
    private final String TAG = this.getClass().toString();

    private final static String X_TIPLY_CLIENT = "X-Tiply-Client";
    private final static String WWW_TIPLY_CLIENT = "WWW-Tiply-Client";
    private final static String LOCATION = "Location";

    private String mAuthorizationToken;
    private String mFacebookToken;

    public void clearAuthorizationToken() {
        mAuthorizationToken = null;
    }

    private TiplyRESTService mRESTService;

    public String getFacebookToken() {
        return mFacebookToken;
    }

    public void setFacebookToken(String mFacebookToken) {
        this.mFacebookToken = mFacebookToken;
    }

    public class TiplyRESTServiceInterceptor implements RequestInterceptor {
        @Override
        public void intercept(RequestInterceptor.RequestFacade requestFacade) {
            if (mAuthorizationToken != null) {
                requestFacade.addHeader(X_TIPLY_CLIENT, mAuthorizationToken);
            }
        }
    }

    public interface WebBaseCallback {
        public class Error {
            private int code;
            private String message;

            public Error(int code, String message) {
                this.code = code;
                this.message = message;
            }

            public int getCode() {
                return code;
            }

            public String getErrorMessage() {
                return message;
            }
        }

        public void onError(final Error error);  // Return code, some object and error message
    }

    public interface WebCallback<T> extends WebBaseCallback {
        public void onSuccess(final T result);
    }

    public interface WebEmptyCallback extends WebBaseCallback {
        public void onSuccess();
    }

    public WebAPIManager() {
        mRESTService = TiplyRESTServiceFactory.tiplyRESTService(new TiplyRESTServiceInterceptor());
    }

    public void login(String email, String password, final WebCallback<Integer> callback) {
        if (callback == null) {
            return;
        }

        mRESTService.login(email, password, "form", new retrofit.Callback<Object>() {
            @Override
            public void success(Object o, retrofit.client.Response response) {
                Log.d(TAG, "Login server answer is OK");
                if (response.getStatus() == 200) {
                    findTokenInHeaders(response);
                }
                callback.onSuccess(response.getStatus());
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(TAG, "Login server answer is Fail: " + (error.getMessage() != null ? error.getMessage() : "UNKNOWN"));
                Response response = error.getResponse();
                callback.onSuccess(response != null ? response.getStatus() : -1);
            }
        });
    }

    private void findTokenInHeaders(Response response) {
        if (response == null || response.getHeaders() == null) {
            return;
        }

        List<Header> headers = response.getHeaders();
        for (Header header : headers) {
            if (header.getName() != null && header.getName().equals(WWW_TIPLY_CLIENT)) {
                mAuthorizationToken = header.getValue();
                Log.d(TAG, "New Application Token: " + mAuthorizationToken);
                break;
            }
        }
    }

    public void loginFacebook(final WebCallback<Integer> callback) {
        if (callback == null) {
            return;
        }

        if (mFacebookToken == null) {
            Log.d(TAG, "Facebook token is empty.");
            callback.onSuccess(-1);
            return;
        }

        mRESTService.login(mFacebookToken, "facebook", new retrofit.Callback<Object>() {
            @Override
            public void success(Object o, retrofit.client.Response response) {
                Log.d(TAG, "Login server answer is OK");
                if (response.getStatus() == 200) {
                    findTokenInHeaders(response);
                }
                callback.onSuccess(response.getStatus());
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(TAG, "Login server answer is Fail: " + (error.getMessage() != null ? error.getMessage() : "UNKNOWN"));
                callback.onSuccess(error.getResponse() != null ? error.getResponse().getStatus() : -1);
            }
        });
    }

    public void getUser(final WebCallback<User> callback) {
        if (callback == null) {
            return;
        }

        mRESTService.getUser(new retrofit.Callback<User>() {
            @Override
            public void success(User user, retrofit.client.Response response) {
                Log.d(TAG, "User fetched");
                callback.onSuccess(user);
            }

            @Override
            public void failure(RetrofitError error) {
                if (error != null && error.getMessage() != null) {
                    Log.d(TAG, error.getMessage());
                }
                callback.onError(null);
            }
        });
    }

    public void getUser(final String userId, final WebCallback<TipUser> callback) {
        if (callback == null) {
            return;
        }

        mRESTService.getUser(userId, new retrofit.Callback<TipUser>() {
            @Override
            public void success(TipUser tipUser, retrofit.client.Response response) {
                Log.d(TAG, "User fetched");
                callback.onSuccess(tipUser);
            }

            @Override
            public void failure(RetrofitError error) {
                callback.onError(null);
            }
        });
    }

    public void createUser(User user, Bitmap userPic, final WebCallback<User> callback) {
        if (callback == null) {
            return;
        }

        final ByteArrayOutputStream stream = new ByteArrayOutputStream();
        if (userPic != null) {
            userPic.compress(Bitmap.CompressFormat.PNG, 100, stream);
        }

        mAuthorizationToken = null;
        mRESTService.createUser(user, userPic != null ?
                        new TypedOutput() {
                            @Override
                            public String fileName() {
                                return "";
                            }

                            @Override
                            public String mimeType() {
                                return "application/octet-stream";
                            }

                            @Override
                            public long length() {
                                return stream.size();
                            }

                            @Override
                            public void writeTo(OutputStream out) throws IOException {
                                out.write(stream.toByteArray());
                            }
                        } : null, new retrofit.Callback<User>() {
                    @Override
                    public void success(User user, retrofit.client.Response response) {
                        Log.d(TAG, "OK");
                        if (response.getStatus() == 200) {
                            findTokenInHeaders(response);
                        }
                        callback.onSuccess(user);
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Log.d(TAG, "Error creating user: " + error.getMessage());

                        String errorMsg = null;

                        if (error.getResponse() != null) {
                            String json;
                            try {
                                BufferedReader reader = new BufferedReader(new InputStreamReader(error.getResponse().getBody().in(), "UTF-8"));
                                json = reader.readLine();
                                try {
                                    JSONObject jsonObject = new JSONObject(json);
                                    errorMsg = jsonObject.getString("message");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                        callback.onError(new WebBaseCallback.Error(-1, errorMsg));
                    }
                }
        );
    }

    public void updateUser(User user, final WebCallback<User> callback) {
        mRESTService.updateUser(user, new Callback<User>() {
            @Override
            public void success(User user, Response response) {
                Log.d(TAG, "User data updated");
                if (callback != null) {
                    callback.onSuccess(user);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(TAG, "User updating fail");
                if (callback != null) {
                    callback.onError(null);
                }
            }
        });
    }

    private class BitMapDecoder extends AsyncTask<Void, Void, Bitmap> {
        private BufferedInputStream stream;
        private WebCallback<Bitmap> callback;

        private BitMapDecoder(BufferedInputStream stream, WebCallback<Bitmap> callback) {
            assert stream != null;
            assert callback != null;

            this.stream = stream;
            this.callback = callback;
        }

        @Override
        protected Bitmap doInBackground(Void... voids) {
            BitmapFactory.Options opts = new BitmapFactory.Options();
            opts.inPreferredConfig = Bitmap.Config.RGB_565;
            //opts.inSampleSize = 2;

            return BitmapFactory.decodeStream(stream, null, opts);
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            if (bitmap != null) {
                callback.onSuccess(bitmap);
            } else {
                callback.onError(null);
            }
        }
    }

    public void getUserProfilePicture(final WebCallback<Bitmap> callback) {
        if (callback == null) {
            return;
        }

        // TODO: Need to cache user profile image
        mRESTService.getUserProfilePicture(new retrofit.Callback<Response>() {
            @Override
            public void success(final Response o, retrofit.client.Response response) {
                if (response.getStatus() == 200) {
                    try {
                        BufferedInputStream is = new BufferedInputStream(response.getBody().in());
                        new BitMapDecoder(is, callback).execute();
                    } catch (IOException e) {
                        e.printStackTrace();
                        callback.onError(null);
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {
                callback.onError(null);
            }
        });
    }

    public void getUserProfilePicture(final String picId, Integer width, Integer height, final WebCallback<Bitmap> callback) {
        if (callback == null) {
            return;
        }

        Callback<Response> responseCallback = new Callback<Response>() {
            @Override
            public void success(Response o, Response response) {
                if (response.getStatus() == 200) {
                    try {
                        BufferedInputStream is = new BufferedInputStream(response.getBody().in());
                        new BitMapDecoder(is, callback).execute();
                    } catch (IOException e) {
                        e.printStackTrace();
                        callback.onError(null);
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {
                callback.onError(null);
            }
        };

        if (width != null && height != null) {
            mRESTService.getUserProfilePicture(picId, width, height, responseCallback);
        } else {
            mRESTService.getUserProfilePicture(picId, responseCallback);
        }

    }

    public void updateUserProfilePicture(Bitmap bitmap, final WebEmptyCallback callback) {
        final ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);

        mRESTService.updateUserProfilePicture(new TypedOutput() {
            @Override
            public String fileName() {
                return "";
            }

            @Override
            public String mimeType() {
                return "application/octet-stream";
            }

            @Override
            public long length() {
                return stream.size();
            }

            @Override
            public void writeTo(OutputStream out) throws IOException {
                out.write(stream.toByteArray());
            }
        }, new Callback<Object>() {
            @Override
            public void success(Object o, Response response) {
                Log.d(TAG, "profile picture updated");
                if (callback != null) {
                    callback.onSuccess();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(TAG, "profile picture updating failure");
                if (callback != null) {
                    callback.onError(null);
                }
            }
        });
    }

    public void replaceUserPhotos(List<Bitmap> bitmaps, final WebEmptyCallback callback) {
        MultipartTypedOutput multipartTypedOutput = new MultipartTypedOutput();

        for (Bitmap bitmap : bitmaps) {
            final ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);

            TypedOutput output = new TypedOutput() {
                @Override
                public String fileName() {
                    return "";
                }

                @Override
                public String mimeType() {
                    return "application/octet-stream";
                }

                @Override
                public long length() {
                    return stream.size();
                }

                @Override
                public void writeTo(OutputStream out) throws IOException {
                    out.write(stream.toByteArray());
                }
            };
            multipartTypedOutput.addPart("photos", output);
        }

        mRESTService.replaceUserPhotos(multipartTypedOutput, new Callback<Object>() {
            @Override
            public void success(Object o, Response response) {
                if (callback != null) {
                    if (response.getStatus() == 200) {
                        callback.onSuccess();
                    } else {
                        callback.onError(null);
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (callback != null) {
                    callback.onError(null);
                }
            }
        });
    }

    public void getUserPhotoStatus(final WebCallback<List<UserPhotosStatus>> callback) {
        if (callback == null) {
            return;
        }

        mRESTService.getUserPhotosStatus(new Callback<UserPhotosStatuses>() {
            @Override
            public void success(UserPhotosStatuses status, Response response) {
                if (response.getStatus() == 200 && status.photos != null) {
                    callback.onSuccess(status.photos);
                } else {
                    callback.onError(null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d("getUserPhotoStatus", error != null ? error.getMessage() : "NULL ERROR");
                callback.onError(null);
            }
        });
    }

    public void getRecentTips(final WebCallback<List<TipUser>> callback) {
        if (callback == null) {
            return;
        }

        mRESTService.getRecentTips(new retrofit.Callback<List<TipUser>>() {
            @Override
            public void success(List<TipUser> tipUsers, retrofit.client.Response response) {
                Log.d(TAG, "Received " + tipUsers.size() + " tips.");
                callback.onSuccess(tipUsers);
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(TAG, "Error fetching tips");
                callback.onError(null);
            }
        });
    }

    public void getRecentTips(double latitude, double longitude, final WebCallback<List<TipUser>> callback) {
        if (callback == null) {
            return;
        }

        mRESTService.getRecentTips(latitude, longitude, new retrofit.Callback<List<TipUser>>() {
            @Override
            public void success(List<TipUser> tipUsers, retrofit.client.Response response) {
                Log.d(TAG, "Received " + tipUsers.size() + " tips.");
                callback.onSuccess(tipUsers);
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(TAG, "Error fetching tips");
                callback.onError(null);
            }
        });
    }

    public void getNearbyTips(final WebCallback<List<TipUser>> callback) {
        if (callback == null) {
            return;
        }

        mRESTService.getNearbyTips(new retrofit.Callback<List<TipUser>>() {
            @Override
            public void success(List<TipUser> tipUsers, retrofit.client.Response response) {
                Log.d(TAG, "Received " + tipUsers.size() + " tips.");
                callback.onSuccess(tipUsers);
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(TAG, "Error fetching tips");
                callback.onError(null);
            }
        });
    }

    public void getNearbyTips(double latitude, double longitude, final WebCallback<List<TipUser>> callback) {
        mRESTService.getNearbyTips(latitude, longitude, new retrofit.Callback<List<TipUser>>() {
            @Override
            public void success(List<TipUser> tipUsers, retrofit.client.Response response) {
                Log.d(TAG, "Received " + tipUsers.size() + " tips.");
                callback.onSuccess(tipUsers);
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(TAG, "Error fetching tips");
                callback.onError(null);
            }
        });
    }

    public void getUserPaymentMethod(final WebCallback<List<PaymentMethod>> callback) {
        if (callback == null) {
            return;
        }

        mRESTService.getUserPaymentMethods(new retrofit.Callback<List<PaymentMethod>>() {
            @Override
            public void success(List<PaymentMethod> paymentMethods, retrofit.client.Response response) {
                Log.d(TAG, "User cards fetched.");
                callback.onSuccess(paymentMethods);
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(TAG, "User cards fetching failed.");
                callback.onError(null);
            }
        });
    }

    public void verifyAccount(String cardId, String amount, final WebCallback<PaymentMethod.VerifyStatus> callback) {
        mRESTService.verifyAccount("application/json", cardId, amount, new Callback<Object>() {
            @Override
            public void success(Object o, Response response) {
                Log.d("verifyAccount OK", String.valueOf(response.getStatus()));
                if (callback != null) {
                    if (response.getStatus() == 200) {
                        callback.onSuccess(PaymentMethod.VerifyStatus.VERIFIED);
                    } else {
                        callback.onSuccess(PaymentMethod.VerifyStatus.ERROR);
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d("verifyAccount error", error.getMessage());
                if (callback != null) {
                    try {
                        if (error.getResponse().getStatus() == 403) {
                            callback.onSuccess(PaymentMethod.VerifyStatus.BLOCKED);
                        } else {
                            callback.onSuccess(PaymentMethod.VerifyStatus.ERROR);
                        }
                    } catch (NullPointerException e) {
                        callback.onSuccess(PaymentMethod.VerifyStatus.ERROR);
                    }
                }
            }
        });
    }

    public void getUserPaymentMethod(String cardId, final WebCallback<PaymentMethod> callback) {
        if (callback == null) {
            return;
        }

        mRESTService.getUserPaymentMethod(cardId, new Callback<PaymentMethod>() {
            @Override
            public void success(PaymentMethod paymentMethod, Response response) {
                callback.onSuccess(paymentMethod);
            }

            @Override
            public void failure(RetrofitError error) {
                callback.onError(null);
            }
        });
    }

    public void createPaymentMethod(PaymentMethod method, final WebCallback<PaymentMethod.Holder> callback) {
        mRESTService.createPaymentMethod(method, new Callback<List<PaymentMethod>>() {
            @Override
            public void success(List<PaymentMethod> paymentMethods, Response response) {
                if (response.getStatus() == 201 && callback != null) {
                    String location = null;

                    List<Header> headers = response.getHeaders();
                    for (Header header : headers) {
                        String name = header.getName();
                        if (name != null && name.equals(LOCATION)) {
                            location = header.getValue();
                            break;
                        }
                    }

                    Log.d(TAG, "Added payment method: " + location);

                    callback.onSuccess(new PaymentMethod.Holder(paymentMethods, location));
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (callback != null) {
                    if (error != null && error.getMessage() != null) {
                        Log.d(TAG, "Payment method didn't add: " + error.getMessage());
                        Response response = error.getResponse();
                        callback.onError(new WebBaseCallback.Error(response != null ? response.getStatus() : -1, null));
                    } else {
                        callback.onError(null);
                    }
                }
            }
        });
    }

    public void updatePaymentMethod(final PaymentMethod method, final WebCallback<PaymentMethod> callback) {
        mRESTService.getUserPaymentMethod(method.id, new Callback<PaymentMethod>() {
            @Override
            public void success(PaymentMethod paymentMethod, Response response) {
                method.version = paymentMethod.version;
                mRESTService.updatePaymentMethod(method.id, PaymentMethod.methodForUpdate(method), new Callback<PaymentMethod>() {
                    @Override
                    public void success(PaymentMethod method, Response response) {
                        if (callback != null) {
                            callback.onSuccess(method);
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        if (callback != null) {
                            callback.onError(null);
                        }
                    }
                });
            }

            @Override
            public void failure(RetrofitError error) {
                if (callback != null) {
                    callback.onError(null);
                }
            }
        });
    }

    public void deletePaymentMethod(String id, final WebEmptyCallback callback) {
        // Do not remove this comment. Remove only if this issue is fixed.
        // Header should be added, because it needed by server

        mRESTService.deletePaymentMethod("application/json; charset=utf-8", id, new Callback<Object>() {
            @Override
            public void success(Object o, Response response) {
                if (callback != null) {
                    callback.onSuccess();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (callback != null) {
                    callback.onError(null);
                }
            }
        });
    }

    @Deprecated
    public void getTipHistory(final WebCallback<TipHistory> callback) {
        if (callback == null) {
            return;
        }

        mRESTService.getTipHistory(new retrofit.Callback<TipHistory>() {
            @Override
            public void success(TipHistory tipHistories, retrofit.client.Response response) {
                Log.d(TAG, "Tip history received");
                callback.onSuccess(tipHistories);
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(TAG, "Tip history receiving failed");
                callback.onError(null);
            }
        });
    }

    public void hideTip(String tipId, final WebEmptyCallback callback) {
        if (callback == null) {
            return;
        }
        mRESTService.hideTip("application/json", tipId, new Callback<Object>() {
            @Override
            public void success(Object o, Response response) {
                callback.onSuccess();
            }

            @Override
            public void failure(RetrofitError error) {
                callback.onError(null);
            }
        });
    }

    public void hideSharedTip(String tipId, final WebEmptyCallback callback) {
        if (callback == null) {
            return;
        }
        mRESTService.hideSharedTip("application/json", tipId, new Callback<Object>() {
            @Override
            public void success(Object o, Response response) {
                callback.onSuccess();
            }

            @Override
            public void failure(RetrofitError error) {
                callback.onError(null);
            }
        });
    }
    
    public void getAllFavoritePersons(final WebCallback<List<TipUser>> callback) {
        if (callback == null) {
            return;
        }

        mRESTService.getAllFavoritePersons(new retrofit.Callback<List<TipUser>>() {
            @Override
            public void success(List<TipUser> tipUsers, retrofit.client.Response response) {
                Log.d(TAG, "Favorites received");
                callback.onSuccess(tipUsers);
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(TAG, "Favorites receiving failed.");
                callback.onError(null);
            }
        });
    }

    public void addFavoritePerson(final TipUser user, final WebCallback<List<TipUser>> callback) {
        mRESTService.addFavoritePerson("application/json", user.id, new retrofit.Callback<List<TipUser>>() {
            @Override
            public void success(List<TipUser> tipUsers, retrofit.client.Response response) {
                if (callback != null) {
                    if ((response.getStatus() == 200 || response.getStatus() == 201)) {
                        callback.onSuccess(tipUsers);
                    } else {
                        callback.onError(null);
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {
                callback.onError(null);
            }
        });
    }

    public void removeFavoritePerson(final TipUser user, final WebCallback<List<TipUser>> callback) {
        mRESTService.deleteFavoritePerson("application/json", user.id, new retrofit.Callback<List<TipUser>>() {
            @Override
            public void success(List<TipUser> tipUsers, retrofit.client.Response response) {
                if (callback != null) {
                    if (response.getStatus() == 200) {
                        callback.onSuccess(tipUsers);
                    } else {
                        callback.onError(null);
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (callback != null) {
                    callback.onError(null);
                }
            }
        });
    }

    public void getAllUserComments(final WebCallback<AllComments> callback) {
        mRESTService.getAllUserComments(new retrofit.Callback<AllComments>() {
            @Override
            public void success(AllComments allComments, retrofit.client.Response response) {
                Log.d(TAG, "Comments received");
                if (callback != null) {
                    callback.onSuccess(allComments);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(TAG, "Comments receiving failed.");
                if (callback != null) {
                    callback.onError(null);
                }
            }
        });
    }

    public void getAllReceivedComments(final WebCallback<List<Comment>> callback) {
        if (callback == null) {
            return;
        }
        mRESTService.getAllUserReceivedComments(new retrofit.Callback<List<Comment>>() {
            @Override
            public void success(List<Comment> comments, retrofit.client.Response response) {
                Log.d(TAG, "Received comments received");
                callback.onSuccess(comments);
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(TAG, "Received comments receiving failed.");
                callback.onError(null);
            }
        });
    }

    public void getAllGivenComments(final WebCallback<List<Comment>> callback) {
        if (callback == null) {
            return;
        }
        mRESTService.getAllUserGivenComments(new retrofit.Callback<List<Comment>>() {
            @Override
            public void success(List<Comment> comments, retrofit.client.Response response) {
                Log.d(TAG, "Given comments received");
                callback.onSuccess(comments);
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(TAG, "Given comments receiving failed.");
                callback.onError(null);
            }
        });
    }

    private void commentsFetched(AllComments comments, WebCallback<AllComments> callback) {
        if (comments != null) {
//            Collections.sort(comments.received, new Comparator<Comment>() {
//                @Override
//                public int compare(Comment lhs, Comment rhs) {
//                    return -lhs.commentedOn.compareTo(rhs.commentedOn);
//                }
//            });

            callback.onSuccess(comments);
        } else {
            callback.onError(null);
        }
    }

    public void getAllUserComments(String userId, final WebCallback<AllComments> callback) {
        if (callback == null) {
            return;
        }

        mRESTService.getAllUserComments(userId, new retrofit.Callback<AllComments>() {
            @Override
            public void success(AllComments allComments, retrofit.client.Response response) {
                Log.d(TAG, "User comments fetched");
                commentsFetched(allComments, callback);
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(TAG, "User comments fetching failed");
                callback.onError(null);
            }
        });
    }

    public void getAllUserReceivedComments(String userId, final WebCallback<AllComments> callback) {
        if (callback == null) {
            return;
        }

        mRESTService.getAllUserReceivedComments(userId, new retrofit.Callback<AllComments>() {
            @Override
            public void success(AllComments allComments, retrofit.client.Response response) {
                Log.d(TAG, "Received comments received");
                commentsFetched(allComments, callback);
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(TAG, "Received comments receiving failed.");
                callback.onError(null);
            }
        });
    }

    public void getAllUserGivenComments(String userId, final WebCallback<AllComments> callback) {
        if (callback == null) {
            return;
        }

        mRESTService.getAllUserGivenComments(userId, new retrofit.Callback<AllComments>() {
            @Override
            public void success(AllComments allComments, retrofit.client.Response response) {
                Log.d(TAG, "Given comments received");
                commentsFetched(allComments, callback);
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(TAG, "Given comments receiving failed.");
                callback.onError(null);
            }
        });
    }

    public void getUserCommentsStatistics(String userId, final WebCallback<UserCommentsStatistic> callback) {
        if (callback == null) {
            return;
        }

        mRESTService.getUserCommentsStatistics(userId, new retrofit.Callback<UserCommentsStatistic>() {
            @Override
            public void success(UserCommentsStatistic userCommentsStatistic, retrofit.client.Response response) {
                Log.d(TAG, "User comments statistics fetched");
                callback.onSuccess(userCommentsStatistic);
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(TAG, "User comments statistics fetching failed.");
                callback.onError(null);
            }
        });
    }

    public void getBusiness(String id, final WebCallback<Business> callback) {
        if (callback == null) {
            return;
        }

        mRESTService.getBusiness(id, new Callback<Business>() {
            @Override
            public void success(Business business, Response response) {
                callback.onSuccess(business);
            }

            @Override
            public void failure(RetrofitError error) {
                callback.onError(null);
            }
        });
    }

    public void updateUserLocation(double latitude, double longitude, final WebEmptyCallback callback) {
        mRESTService.updateUserLocation("application/json", latitude, longitude, new retrofit.Callback<Object>() {
            @Override
            public void success(Object o, retrofit.client.Response response) {
                if (callback != null) {
                    if (response.getStatus() == 200) {
                        callback.onSuccess();
                    } else {
                        callback.onError(null);
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (callback != null) {
                    callback.onError(null);
                }
            }
        });
    }

    public void searchPeople(String query, String zip, final WebCallback<List<TipUser>> callback) {
        if (callback == null) {
            return;
        }

        mRESTService.searchPeople(query, zip, new retrofit.Callback<List<TipUser>>() {
            @Override
            public void success(List<TipUser> tipUsers, retrofit.client.Response response) {
                if (response.getStatus() == 200) {
                    callback.onSuccess(tipUsers);
                } else {
                    callback.onError(null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                callback.onError(null);
            }
        });
    }

    public void addComment(String userId, final Comment comment, final WebCallback<Comment> callback) {
        mRESTService.addComment(userId, comment, new retrofit.Callback<Comment>() {
            @Override
            public void success(Comment comment, retrofit.client.Response response) {
                if (callback != null) {
                    if (response.getStatus() == 201) {
                        callback.onSuccess(comment);
                    } else {
                        callback.onError(null);
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (callback != null) {
                    callback.onError(null);
                }
            }
        });
    }

    public void registerFacebookToken(String token, final WebEmptyCallback callback) {
        mRESTService.registerFacebookToken(token, new Callback<Object>() {
            @Override
            public void success(Object o, Response response) {
                if (callback != null) {
                    callback.onSuccess();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (callback != null) {
                    callback.onError(null);
                }
            }
        });
    }

    public void getAllCustomLocationModel(final WebCallback<List<CustomLocation>> callback) {
        if (callback == null) {
            return;
        }

        mRESTService.getAllCustomLocations(new Callback<List<CustomLocation>>() {
            @Override
            public void success(List<CustomLocation> locations, Response response) {
                callback.onSuccess(locations);
            }

            @Override
            public void failure(RetrofitError error) {
                callback.onError(null);
            }
        });
    }

    public void createCustomLocation(final CustomLocation location, final WebCallback<CustomLocation> callback) {
        mRESTService.createCustomLocation(location, new Callback<Object>() {
            @Override
            public void success(Object o, Response response) {
                if (callback != null) {
                    callback.onSuccess(response.getStatus() == 201 ? location : null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (callback != null) {
                    callback.onError(null);
                }
            }
        });
    }

    public void getCustomLocation(long locationId, final WebCallback<CustomLocation> callback) {
        if (callback == null) {
            return;
        }

        mRESTService.getCustomLocation(locationId, new Callback<CustomLocation>() {
            @Override
            public void success(CustomLocation location, Response response) {
                callback.onSuccess(location);
            }

            @Override
            public void failure(RetrofitError error) {
                callback.onError(null);
            }
        });
    }

    public void updateCustomLocation(CustomLocation location, final WebCallback<CustomLocation> callback) {
        mRESTService.updateCustomLocation(location.id, location, new Callback<CustomLocation>() {
            @Override
            public void success(CustomLocation location, Response response) {
                if (callback != null) {
                    callback.onSuccess(location);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (callback != null) {
                    callback.onError(null);
                }
            }
        });
    }

    public void deleteCustomLocation(long locationId, final WebEmptyCallback callback) {
        mRESTService.deleteCustomLocation(locationId, new Callback<Object>() {
            @Override
            public void success(Object o, Response response) {
                if (callback != null) {
                    callback.onSuccess();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (callback != null) {
                    callback.onError(null);
                }
            }
        });
    }

    public void requestPasswordReset(String email, final WebEmptyCallback callback) {
        mRESTService.requestPasswordReset(email, new Callback<Object>() {
            @Override
            public void success(Object o, Response response) {
                if (response.getStatus() == 200 && callback != null) {
                    callback.onSuccess();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (callback != null) {
                    callback.onError(null);
                }
            }
        });
    }

    public void getUserSettings(final WebCallback<UserSettings> callback) {
        if (callback == null) {
            return;
        }
        mRESTService.getUserSettings(new Callback<UserSettings>() {
            @Override
            public void success(UserSettings settings, Response response) {
                callback.onSuccess(settings);
            }

            @Override
            public void failure(RetrofitError error) {
                callback.onError(null);
            }
        });
    }

    public void updateUserSettings(UserSettings settings, final WebEmptyCallback callback) {
        mRESTService.updateUserSettings(settings, new Callback<Object>() {
            @Override
            public void success(Object o, Response response) {
                if (callback != null) {
                    callback.onSuccess();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (callback != null) {
                    callback.onError(null);
                }
            }
        });
    }

    public void createTip(TipRequest request, final WebCallback<TipRequest> callback) {
        mRESTService.createTip(TipRequest.tipRequestForCreate(request), new Callback<TipRequest>() {
            @Override
            public void success(TipRequest request, Response response) {
                if (callback != null) {
                    callback.onSuccess(request);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (callback != null) {
                    callback.onError(null);
                }
            }
        });
    }

    public void updateTip(TipRequest tipRequest, final WebCallback<TipRequest> callback) {
        JsonObject asJsonObject = TiplyRESTServiceFactory.getGson().toJsonTree(TipRequest.tipRequestForUpdate(tipRequest)).getAsJsonObject();
        asJsonObject.remove("recipientId");
        asJsonObject.remove("id");

        mRESTService.updateTip(tipRequest.id, asJsonObject, new Callback<TipRequest>() {
            @Override
            public void success(TipRequest tipRequest, Response response) {
                if (callback != null) {
                    callback.onSuccess(tipRequest);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (callback != null) {
                    callback.onError(null);
                }
            }
        });
    }

    public void createTip(TipRequest tipRequest, Bitmap picture, final WebCallback<TipRequest> callback) {
        if (callback == null) {
            return;
        }

        if (picture == null || tipRequest == null) {
            callback.onError(null);
            return;
        }

        final ByteArrayOutputStream stream = new ByteArrayOutputStream();
        picture.compress(Bitmap.CompressFormat.PNG, 100, stream);

        mRESTService.createTip(TipRequest.tipRequestForCreate(tipRequest), new TypedOutput() {
            @Override
            public String fileName() {
                return "";
            }

            @Override
            public String mimeType() {
                return "application/octet-stream";
            }

            @Override
            public long length() {
                return stream.size();
            }

            @Override
            public void writeTo(OutputStream out) throws IOException {
                out.write(stream.toByteArray());
            }
        }, new Callback<TipRequest>() {
            @Override
            public void success(TipRequest tipRequest, Response response) {
                callback.onSuccess(tipRequest);
            }

            @Override
            public void failure(RetrofitError error) {
                callback.onError(null);
            }
        });
    }

    public void getFacebookPhoto(String userId, final WebCallback<Bitmap> callback) {
        if (callback == null) {
            return;
        }

        if (userId == null) {
            callback.onError(null);
        }

        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url("https://graph.facebook.com/" + userId + "/picture?height=400&width=400")
                .build();

        final Call call = client.newCall(request);
        call.enqueue(new com.squareup.okhttp.Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                callback.onError(null);
            }

            @Override
            public void onResponse(com.squareup.okhttp.Response response) throws IOException {
                BufferedInputStream is = new BufferedInputStream(response.body().byteStream());
                new BitMapDecoder(is, callback).execute();
            }
        });
    }

    public void getTipRequestDetails(String tipId, final WebCallback<TipRequest> callback) {
        if (callback == null) {
            return;
        }

        mRESTService.getTipRequestDetails(tipId, new Callback<TipRequest>() {
            @Override
            public void success(TipRequest tipRequest, Response response) {
                callback.onSuccess(tipRequest);
            }

            @Override
            public void failure(RetrofitError error) {
                callback.onError(null);
            }
        });
    }

    public void getTipRequestsInProgress(final WebCallback<List<TipRequest>> callback) {
        if (callback == null) {
            return;
        }
        mRESTService.getTipRequestsInProgress(new Callback<List<TipRequest>>() {
            @Override
            public void success(List<TipRequest> tipRequests, Response response) {
                callback.onSuccess(tipRequests);
            }

            @Override
            public void failure(RetrofitError error) {
                callback.onError(null);
            }
        });
    }

    public void cancelTip(String tipId, final WebEmptyCallback callback) {
        if (callback == null || tipId == null || tipId.length() == 0) {
            return;
        }

        mRESTService.cancelTip(tipId, new Callback<Object>() {
            @Override
            public void success(Object o, Response response) {
                if (response.getStatus() == 200) {
                    callback.onSuccess();
                } else {
                    callback.onError(null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                callback.onError(null);
            }
        });
    }

    public void confirmTip(String tipId, String paymentMethodId, final WebCallback<TipUser.Status> callback) {
        if (callback == null || tipId == null || tipId.length() == 0) {
            return;
        }

        mRESTService.confirmTip("application/json", tipId, paymentMethodId, new Callback<Object>() {
            @Override
            public void success(Object o, Response response) {
                callback.onSuccess(TipUser.Status.OK);
            }

            @Override
            public void failure(RetrofitError error) {
                if (error.isNetworkError()) {
                    callback.onError(null);
                } else {
                    int code = error.getResponse().getStatus();
                    if (code == 404) {
                        callback.onSuccess(TipUser.Status.NOT_FOUND);
                    } else if (code == 403) {
                        callback.onSuccess(TipUser.Status.FORBIDDEN);
                    } else {
                        callback.onError(null);
                    }
                }
            }
        });
    }

    public void createAnonymousTipRequest(AnonymousTipRequest request, final WebCallback<AnonymousTipRequest> callback) {
        if (callback == null || request == null) {
            return;
        }
        JsonObject asJsonObject = TiplyRESTServiceFactory.getGson().toJsonTree(request).getAsJsonObject();
        asJsonObject.remove("version");
        asJsonObject.remove("status");
        asJsonObject.remove("hidden");

        mRESTService.createAnonymousTip(asJsonObject, new Callback<AnonymousTipRequest>() {
            @Override
            public void success(AnonymousTipRequest request, Response response) {
                callback.onSuccess(request);
            }

            @Override
            public void failure(RetrofitError error) {
                callback.onError(null);
            }
        });
    }

    public void queryTipRecipients(double latitude, double longitude, final WebCallback<List<TipUser>> callback) {
        if (callback == null) {
            return;
        }

        Callback<List<TipUser>> tipCallback = new Callback<List<TipUser>>() {
            @Override
            public void success(List<TipUser> tipUsers, Response response) {
                callback.onSuccess(tipUsers);
            }

            @Override
            public void failure(RetrofitError error) {
                callback.onError(null);
            }
        };

        if (latitude == 0.0f || longitude == 0.0f) {
            mRESTService.queryTipRecipients(tipCallback);
        } else {
            mRESTService.queryTipRecipients(latitude, longitude, tipCallback);
        }
    }

    public void updateUserTimezone(UpdateUserTimeZone tz, final WebEmptyCallback callback) {
        mRESTService.updateTimeZone(tz, new Callback<Object>() {
            @Override
            public void success(Object o, Response response) {
                if (callback != null) {
                    callback.onSuccess();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (callback != null) {
                    callback.onError(null);
                }
            }
        });
    }

    public void requestBankAccountVerification(String id, final WebEmptyCallback callback) {
        mRESTService.requestVerifyAccount("application/json", id, new Callback<Object>() {
            @Override
            public void success(Object o, Response response) {
                Log.d("Bank Account Verify", String.valueOf(response.getStatus()));
                if (callback != null) {
                    callback.onSuccess();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d("Bank Account Verify", error.getMessage());
                if (callback != null) {
                    callback.onError(null);
                }
            }
        });
    }

    public void getUserPhoto(String id, Integer width, Integer height, final WebCallback<Bitmap> callback) {
        if (callback == null || id == null) {
            return;
        }

        Callback<Response> responseCallback = new Callback<Response>() {
            @Override
            public void success(Response response, Response response2) {
                try {
                    BufferedInputStream is = new BufferedInputStream(response.getBody().in());
                    new BitMapDecoder(is, callback).execute();
                } catch (IOException e) {
                    e.printStackTrace();
                    callback.onError(null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                callback.onError(null);
            }
        };

        if (width != null && height != null) {
            mRESTService.getUserPhoto(id, width, height, responseCallback);
        } else {
            mRESTService.getUserPhoto(id, responseCallback);
        }
    }

    public void markAllCommentsRead(final WebEmptyCallback callback) {
        mRESTService.markAllCommentsRead("application/json", new Callback<Object>() {
            @Override
            public void success(Object o, Response response) {
                if (callback != null) {
                    callback.onSuccess();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (callback != null) {
                    callback.onError(null);
                }
            }
        });
    }

    public void getAllUnreadComments(Integer limit, Integer offset, final WebCallback<List<Comment>> callback) {
        if (callback == null) {
            return;
        }

        mRESTService.getAllUnreadComments(limit, offset, new Callback<List<Comment>>() {
            @Override
            public void success(List<Comment> comments, Response response) {
                callback.onSuccess(comments);
            }

            @Override
            public void failure(RetrofitError error) {
                callback.onError(null);
            }
        });
    }

    public void updateToken(final WebEmptyCallback callback) {
        mRESTService.updateToken(new Callback<Object>() {
            @Override
            public void success(Object o, Response response) {
                if (response.getStatus() == 200) {
                    findTokenInHeaders(response);
                }
                if (callback != null) {
                    callback.onSuccess();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (callback != null) {
                    callback.onError(null);
                }
            }
        });
    }

    public void markAllTipsRead(final WebEmptyCallback callback) {
        mRESTService.markAllTipsRead("application/json", new Callback<Object>() {
            @Override
            public void success(Object o, Response response) {
                if (callback != null) {
                    callback.onSuccess();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (callback != null) {
                    callback.onError(null);
                }
            }
        });
    }

    public void getAllUnviewedTips(final WebCallback<List<TipHistoryEntry>> callback) {
        if (callback == null) {
            return;
        }
        mRESTService.getUnviewedTips(new Callback<List<TipHistoryEntry>>() {
            @Override
            public void success(List<TipHistoryEntry> tipHistoryEntries, Response response) {
                callback.onSuccess(tipHistoryEntries);
            }

            @Override
            public void failure(RetrofitError error) {
                callback.onError(null);
            }
        });
    }

    public void getTipHistoryTipped(final WebCallback<List<TipHistoryEntry>> callback) {
        if (callback == null) {
            return;
        }

        mRESTService.getTipHistoryTipped(new Callback<List<TipHistoryEntry>>() {
            @Override
            public void success(List<TipHistoryEntry> tipHistoryEntries, Response response) {
                callback.onSuccess(tipHistoryEntries);
            }

            @Override
            public void failure(RetrofitError error) {
                callback.onError(null);
            }
        });
    }

    public void getTipHistoryReceived(final WebCallback<List<TipHistoryEntry>> callback) {
        if (callback == null) {
            return;
        }

        mRESTService.getTipHistoryReceived(new Callback<List<TipHistoryEntry>>() {
            @Override
            public void success(List<TipHistoryEntry> tipHistoryEntries, Response response) {
                callback.onSuccess(tipHistoryEntries);
            }

            @Override
            public void failure(RetrofitError error) {
                callback.onError(null);
            }
        });
    }

    public void getTipHistoryTippedStatistics(final WebCallback<TipStatisticsOnly> callback) {
        if (callback == null) {
            return;
        }

        mRESTService.getTipHistoryTippedStatistics(new Callback<TipStatisticsOnly>() {
            @Override
            public void success(TipStatisticsOnly tipStatisticsOnly, Response response) {
                callback.onSuccess(tipStatisticsOnly);
            }

            @Override
            public void failure(RetrofitError error) {
                callback.onError(null);
            }
        });

    }

    public void getTipHistoryReceivedStatistics(final WebCallback<TipStatisticsOnly> callback) {
        if (callback == null) {
            return;
        }

        mRESTService.getTipHistoryReceivedStatistics(new Callback<TipStatisticsOnly>() {
            @Override
            public void success(TipStatisticsOnly tipStatisticsOnly, Response response) {
                callback.onSuccess(tipStatisticsOnly);
            }

            @Override
            public void failure(RetrofitError error) {
                callback.onError(null);
            }
        });

    }

    public void searchPool(String searchText, final WebCallback<List<Pool>> callback) {
        if (callback == null || searchText == null) {
            return;
        }

        mRESTService.searchPool(searchText, new Callback<List<Pool>>() {
            @Override
            public void success(List<Pool> pools, Response response) {
                callback.onSuccess(pools);
            }

            @Override
            public void failure(RetrofitError error) {
                callback.onError(null);
            }
        });
    }

    public void getRegisteredPools(double latitude, double longitude, final WebCallback<List<TipPool>> callback) {
        mRESTService.getRegisteredPools(latitude, longitude, new Callback<List<TipPool>>() {
            @Override
            public void success(List<TipPool> tipPools, Response response) {
                callback.onSuccess(tipPools);
            }

            @Override
            public void failure(RetrofitError error) {
                callback.onError(null);
            }
        });

    }
}
