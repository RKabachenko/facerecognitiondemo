package ly.tip.webservice;

import com.google.gson.JsonObject;

import java.util.List;

import ly.tip.webservice.restmodels.AllComments;
import ly.tip.webservice.restmodels.AnonymousTipRequest;
import ly.tip.webservice.restmodels.Business;
import ly.tip.webservice.restmodels.Comment;
import ly.tip.webservice.restmodels.CustomLocation;
import ly.tip.webservice.restmodels.PaymentMethod;
import ly.tip.webservice.restmodels.Pool;
import ly.tip.webservice.restmodels.TipHistory;
import ly.tip.webservice.restmodels.TipHistoryEntry;
import ly.tip.webservice.restmodels.TipPool;
import ly.tip.webservice.restmodels.TipRequest;
import ly.tip.webservice.restmodels.TipStatisticsOnly;
import ly.tip.webservice.restmodels.TipUser;
import ly.tip.webservice.restmodels.UpdateUserTimeZone;
import ly.tip.webservice.restmodels.User;
import ly.tip.webservice.restmodels.UserCommentsStatistic;
import ly.tip.webservice.restmodels.UserPhotosStatuses;
import ly.tip.webservice.restmodels.UserSettings;
import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Part;
import retrofit.http.Path;
import retrofit.http.Query;
import retrofit.http.Streaming;
import retrofit.mime.MultipartTypedOutput;
import retrofit.mime.TypedOutput;

public interface TiplyRESTService {
    /*
    User management, Log in, Registration facebook token
     */

    @PUT("/facebook_token")
    void registerFacebookToken(@Body String token, Callback<Object> callback);

    @FormUrlEncoded
    @POST("/login")
    void login(@Field("email") String email, @Field("password") String password, @Field("provider") String provider, Callback<Object> callback);

    @FormUrlEncoded
    @POST("/login")
    void login(@Field("token") String token, @Field("provider") String provider, Callback<Object> callback);

    @GET("/")
    void getUser(Callback<User> callback);

    @GET("/passwordReset")
    void requestPasswordReset(@Query("email") String email, Callback<Object> callback);

    @Multipart
    @POST("/")
    void createUser(@Part("data") User user, @Part("profilePicture") TypedOutput bitmap, Callback<User> callback);

    @GET("/user/{id}")
    void getUser(@Path("id") String userId, Callback<TipUser> callback);

    @PUT("/")
    void updateUser(@Body User user, Callback<User> callback);

    /*
    User profile picture
     */

    @GET("/profilePicture")
    @Streaming
    void getUserProfilePicture(Callback<Response> callback);

    @GET("/profilePicture/{picId}")
    @Streaming
    void getUserProfilePicture(@Path("picId") String pictureId, Callback<Response> callback);

    @GET("/profilePicture/{picId}")
    @Streaming
    void getUserProfilePicture(@Path("picId") String pictureId, @Query("w") Integer width, @Query("h") Integer height, Callback<Response> callback);

    @Multipart
    @POST("/profilePicture")
    void updateUserProfilePicture(@Part("profilePicture") TypedOutput bitmap, Callback<Object> callback);

    /*
    User photos
     */

    @POST("/photos")
    void replaceUserPhotos(@Body MultipartTypedOutput bitmaps, Callback<Object> callback);

    @GET("/photos/status")
    void getUserPhotosStatus(Callback<UserPhotosStatuses> callback);

    @GET("/photos/{id}")
    void getUserPhoto(@Path("id") String id, @Query("w") int width, @Query("h") int height, Callback<Response> callback);

    @GET("/photos/{id}")
    void getUserPhoto(@Path("id") String id, Callback<Response> callback);

    /*
    Tips
     */

    @POST("/tip")
    void createTip(@Body TipRequest tipRequest, Callback<TipRequest> callback);

    @Multipart
    @POST("/tip")
    void createTip(@Part("data") TipRequest tipRequest, @Part("photo") TypedOutput photo, Callback<TipRequest> callback);

    @POST("/tip/anonymous")
    void createAnonymousTip(@Body JsonObject request, Callback<AnonymousTipRequest> callback);

    @PUT("/tip")
    void updateTip(@Query("id") String tipId, @Body JsonObject tipRequest, Callback<TipRequest> callback);

    @GET("/tip")
    void getTipRequestDetails(@Query("id") String tipId, Callback<TipRequest> callback);

    @GET("/tip")
    void getTipRequestsInProgress(Callback<List<TipRequest>> callback);

    @DELETE("/tip")
    void cancelTip(@Query("id") String tipId, Callback<Object> callback);

    @POST("/tip")
    void confirmTip(@Header("Content-Type") String contentType, @Query("id") String tipId, @Query("pmId") String paymentMethodId, Callback<Object> callback);

    @GET("/tip/recent")
    void getRecentTips(Callback<List<TipUser>> callback);

    @GET("/tip/recent")
    void getRecentTips(@Query("t") double latitude, @Query("n") double longitude, Callback<List<TipUser>> callback);

    @GET("/tip/nearby")
    void getNearbyTips(Callback<List<TipUser>> callback);

    @GET("/tip/nearby")
    void getNearbyTips(@Query("t") double latitude, @Query("n") double longitude, Callback<List<TipUser>> callback);

    @Deprecated
    @GET("/tip/history")
    void getTipHistory(Callback<TipHistory> callback);

    @GET("/tip/history/tipped")
    void getTipHistoryTipped(Callback<List<TipHistoryEntry>> callback);

    @GET("/tip/history/received")
    void getTipHistoryReceived(Callback<List<TipHistoryEntry>> callback);

    @GET("/tip/history/tipped/statistic")
    void getTipHistoryTippedStatistics(Callback<TipStatisticsOnly> callback);

    @GET("/tip/history/received/statistic")
    void getTipHistoryReceivedStatistics(Callback<TipStatisticsOnly> callback);
    
    @DELETE("/tip/history")
    void hideTip(@Header("Content-Type") String contentType, @Query("id") String tipId, Callback<Object> callback);

    @DELETE("/tip/history/share/{id}")
    void hideSharedTip(@Header("Content-Type") String contentType, @Path("id") String tipId, Callback<Object> callback);

    @GET("/tip/registered")
    void queryTipRecipients(@Query("t") double latitude, @Query("n") double longitude, Callback<List<TipUser>> callback);

    @GET("/tip/registered")
    void queryTipRecipients(Callback<List<TipUser>> callback);

    @GET("/tip/unread")
    void getUnviewedTips(Callback<List<TipHistoryEntry>> callback);

    @PUT("/tip/markAllRead")
    void markAllTipsRead(@Header("Content-Type") String contentType, Callback<Object> callback);

    /*
    Payment methods
     */

    @GET("/accounts")
    void getUserPaymentMethods(Callback<List<PaymentMethod>> callback);

    @GET("/accounts/{cardId}")
    void getUserPaymentMethod(@Path("cardId") String cardId, Callback<PaymentMethod> callback);

    @POST("/accounts")
    void createPaymentMethod(@Body PaymentMethod method, Callback<List<PaymentMethod>> callback);

    @PUT("/accounts/{cardId}")
    void updatePaymentMethod(@Path("cardId") String cardId, @Body PaymentMethod paymentMethod, Callback<PaymentMethod> callback);

    @DELETE("/accounts/{cardId}")
    void deletePaymentMethod(@Header("Content-Type") String contentType, @Path("cardId") String cardId, Callback<Object> callback);

    @GET("/accounts/{cardId}/verify")
    void requestVerifyAccount(@Header("Content-Type") String contentType, @Path("cardId") String cardId, Callback<Object> callback);

    @POST("/accounts/{cardId}/verify")
    void verifyAccount(@Header("Content-Type") String contentType, @Path("cardId") String cardId, @Query("amount") String amount, Callback<Object> callback);

    /*
    Favorite persons
     */

    @GET("/favorites")
    void getAllFavoritePersons(Callback<List<TipUser>> callback);

    @POST("/favorites/{personId}")
    void addFavoritePerson(@Header("Content-Type") String contentType, @Path("personId") String personId, Callback<List<TipUser>> callback);

    @DELETE("/favorites/{personId}")
    void deleteFavoritePerson(@Header("Content-Type") String contentType, @Path("personId") String personId, Callback<List<TipUser>> callback);

    /*
    Comments
     */

    @POST("/comments")
    void addComment(@Query("userId") String userId, @Body Comment comment, Callback<Comment> callback);

    @GET("/comments")
    void getAllUserComments(Callback<AllComments> callback);

    @GET("/comments/received")
    void getAllUserReceivedComments(Callback<List<Comment>> callback);

    @GET("/comments/given")
    void getAllUserGivenComments(Callback<List<Comment>> callback);

    @GET("/comments/unread")
    void getAllUnreadComments(@Query("limit") Integer limit, @Query("offset") Integer offset, Callback<List<Comment>> callback);

    @PUT("/comments/markAllRead")
    void markAllCommentsRead(@Header("Content-Type") String contentType, Callback<Object> callback);

    @GET("/user/{id}/comments")
    void getAllUserComments(@Path("id") String userId, Callback<AllComments> callback);

    @GET("/user/{id}/comments/received")
    void getAllUserReceivedComments(@Path("id") String userId, Callback<AllComments> callback);

    @GET("/user/{id}/comments/given")
    void getAllUserGivenComments(@Path("id") String userId, Callback<AllComments> callback);

    @GET("/user/{id}/comments/statistic")
    void getUserCommentsStatistics(@Path("id") String userId, Callback<UserCommentsStatistic> callback);

    /*
    People search
     */

    @GET("/people")
    void searchPeople(@Query("q") String query, @Query("z") String zip, Callback<List<TipUser>> callback);

    /*
    Location management
     */

    @PUT("/location")
    void updateUserLocation(@Header("Content-Type") String contentType, @Query("t") double latitude, @Query("n") double longitude, Callback<Object> callback);

    @GET("/customLocations")
    void getAllCustomLocations(Callback<List<CustomLocation>> callback);

    @POST("/customLocations")
    void createCustomLocation(@Body CustomLocation location, Callback<Object> callback);

    @GET("/customLocations/{id}")
    void getCustomLocation(@Path("id") long locationId, Callback<CustomLocation> callback);

    @PUT("/customLocations/{id}")
    void updateCustomLocation(@Path("id") long locationId, @Body CustomLocation location, Callback<CustomLocation> callback);

    @DELETE("/customLocations/{id}")
    void deleteCustomLocation(@Path("id") long locationId, Callback<Object> callback);

    /*
    Business
     */

    @GET("/business")
    void getBusiness(@Query("foursquareId") String id, Callback<Business> callback);

    /*
    Settings
     */

    @GET("/settings")
    void getUserSettings(Callback<UserSettings> callback);

    @PUT("/settings")
    void updateUserSettings(@Body UserSettings settings, Callback<Object> callback);

    /*
    Timezone
     */

    @PUT("/timezone")
    void updateTimeZone(@Body UpdateUserTimeZone timeZone, Callback<Object> callback);

    /*
    Lifecycle
    */

    @GET("refreshSession")
    void updateToken(Callback<Object> callback);
    
    /*
    Pools
     */
    
    @GET("/pools/search")
    void searchPool(@Query("q") String searchString, Callback<List<Pool>> callback);

    @GET("/tip/pools/registered")
    void getRegisteredPools(@Query("t") double latitude, @Query("n") double longitude, Callback<List<TipPool>> callback);

}
