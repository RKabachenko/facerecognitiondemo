package mx.camera.camera;

import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.RectF;
import android.hardware.Camera.Face;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

public class FaceMatcher {

    private RectF mMatchingArea;
    private Matrix faceConvertMatrix;

    public FaceMatcher(Matrix matrix) {
        this.faceConvertMatrix = matrix;
    }

    public Face getTheBestFace(ArrayList<Face> faces) {
        if (mMatchingArea == null || faceConvertMatrix==null)
            return null;

        faces = removeOutsideFaces(faces);
        Collections.sort(faces, faceComparator);
        return faces.size() > 0 ? faces.get(0) : null;
    }

    private ArrayList<Face> removeOutsideFaces(ArrayList<Face> faces) {
        Iterator<Face> iterator = faces.iterator();
        while (iterator.hasNext()) {
            Face face = iterator.next();
            RectF faceRect = new RectF(face.rect);
            faceConvertMatrix.mapRect(faceRect);
            if (!mMatchingArea.contains(faceRect)) {
                iterator.remove();
            }
        }
        return faces;
    }

    private Comparator<Face> faceComparator = new Comparator<Face>() {
        @Override
        public int compare(Face face, Face face2) {

            if (face.score > face2.score)
                return 1;
            else if (face.score < face2.score)
                return -1;

            int faceSquare = getRectSquare(face.rect);
            int faceSquare2 = getRectSquare(face2.rect);

            if (faceSquare > faceSquare2) return 1;
            else if (faceSquare < faceSquare2) return -1;
            return 0;
        }
    };

    private int getRectSquare(Rect rect) {
        return Math.abs(rect.width() * rect.height());
    }


    public void setMatchingArea(RectF matchingArea) {
        this.mMatchingArea = matchingArea;
    }
}
