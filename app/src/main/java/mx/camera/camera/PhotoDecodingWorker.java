package mx.camera.camera;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapRegionDecoder;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.RectF;
import android.hardware.Camera;
import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;

public class PhotoDecodingWorker extends AsyncTask<byte[], Void, Bitmap> {
    private final String TAG = PhotoDecodingWorker.class.getName();
    private DecodingCallback mCallback;
    private CameraHolder.CameraType mType;
    private RectF mDetectionArea;
    private int containerWidth, containerHeight;
    private Camera.Face bestFace;

    public PhotoDecodingWorker(CameraHolder.CameraType type, RectF detectionArea, int width, int height, Camera.Face bestFace, DecodingCallback callback) {
        this.mType = type;
        this.mDetectionArea = detectionArea;
        this.mCallback = callback;
        this.containerWidth = width;
        this.containerHeight = height;
        this.bestFace = bestFace;
    }

    @Override
    protected Bitmap doInBackground(byte[]... params) {
        if (mCallback == null)
            return null;
        return decodePhotoRegion(params[0]);
    }

    @Override
    protected void onPostExecute(Bitmap photo) {
        mCallback.onDecoded(photo);
    }

    private Bitmap decodePhotoRegion(byte[] data) {
        final boolean isCameraFrontFacing = mType == CameraHolder.CameraType.FRONT_FACING_CAMERA;
        BitmapRegionDecoder decoder;

        try {
            decoder = BitmapRegionDecoder.newInstance(data, 0, data.length, false);
        } catch (IOException e) {
            Log.e(TAG, e.getMessage());
            return null;
        }

        int rawPhotoWidth = decoder.getWidth();
        int rawPhotoHeight = decoder.getHeight();

        float scaleX = rawPhotoWidth / (float) containerHeight;
        float scaleY = rawPhotoHeight / (float) containerWidth;

        RectF areaF = new RectF();

        RectF detectionArea = new RectF(mDetectionArea);
        Matrix matrix = new Matrix();
        matrix.preScale(scaleX, scaleY);

        float rx = rawPhotoHeight / 2;
        float ry = rawPhotoHeight / 2;

        matrix.postRotate(isCameraFrontFacing ? 360 - CameraHolder.PREVIEW_ORIENTATION : CameraHolder.PREVIEW_ORIENTATION, rx, ry);
        matrix.mapRect(detectionArea);

        if (bestFace != null) {
            matrix.setScale(isCameraFrontFacing ? -1 : 1, isCameraFrontFacing ? -1 : 1);
            matrix.postScale(rawPhotoWidth / 2000f, rawPhotoHeight / 2000f);
            matrix.postTranslate(rawPhotoWidth / 2f, rawPhotoHeight / 2f);

            areaF.set(bestFace.rect);
            matrix.mapRect(areaF);
            float dw = detectionArea.width() - areaF.width();
            float dh = detectionArea.height() - areaF.height();
            areaF.left -= dw / 2;
            areaF.right += dw / 2;
            areaF.top -= dh / 2;
            areaF.bottom += dh / 2;
        } else {
            areaF.set(detectionArea);
        }

        if (isCameraFrontFacing) {
            matrix.reset();
            matrix.setScale(-1, -1, rawPhotoWidth / 2, rawPhotoHeight / 2);
            matrix.mapRect(areaF);
        }

        Rect area = new Rect();
        areaF.round(area);

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.RGB_565;

        Bitmap photo = decoder.decodeRegion(area, options);
        decoder.recycle();
        System.gc();

        matrix.reset();
        matrix.setRotate(isCameraFrontFacing ? 360 - CameraHolder.PREVIEW_ORIENTATION : CameraHolder.PREVIEW_ORIENTATION);
        if (isCameraFrontFacing)
            matrix.preScale(1, -1);

        Bitmap rotatedPhoto = Bitmap.createBitmap(photo, 0, 0, photo.getWidth(), photo.getHeight(), matrix, true);
        photo.recycle();
        System.gc();
        return rotatedPhoto;
    }

    public interface DecodingCallback {
        void onDecoded(Bitmap bmp);
    }
}
