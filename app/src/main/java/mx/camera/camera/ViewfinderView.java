package mx.camera.camera;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

public class ViewfinderView extends View {

    private float mRadius, mX, mY;
    private boolean initialized = false;
    private Paint mPaint, mTransparentPaint;

    public ViewfinderView(Context context) {
        super(context);
    }

    public ViewfinderView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ViewfinderView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void init(int color) {
        mPaint = getBackgroundPaint(color);
        mTransparentPaint = getTransparentPaint();
        initialized = true;
        setWillNotDraw(false);
    }

    public void setFinderRect(RectF rect) {
        mRadius = rect.width() / 2;
        mX = rect.left + mRadius;
        mY = rect.top + mRadius;
        invalidate();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    private Paint getTransparentPaint() {
        Paint transparentPaint = new Paint();
        transparentPaint.setColor(getResources().getColor(android.R.color.transparent));
        transparentPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
        return transparentPaint;
    }

    private Paint getBackgroundPaint(int color) {
        Paint paint = new Paint();
        paint.setColor(color);
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.FILL);
        paint.setDither(true);
        return paint;
    }

    private void drawFinder(Canvas canvas) {
        Bitmap buffer = Bitmap.createBitmap(canvas.getWidth(), canvas.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas image = new Canvas(buffer);
        image.drawRect(0, 0, image.getWidth(), image.getHeight(), mPaint);
        image.drawCircle(mX, mY, mRadius, mTransparentPaint);

        canvas.drawBitmap(buffer, 0, 0, new Paint());
        buffer.recycle();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if (initialized) {
            drawFinder(canvas);
        }
        super.onDraw(canvas);
    }
}
