package mx.camera.camera;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.util.AttributeSet;
import android.view.TextureView;
import android.widget.FrameLayout;

import com.privatas.facerecognitiondemo.R;

import java.util.List;


public class FaceDetectionView extends FrameLayout {

    private static final boolean DEBUG_MODE = true;

    private static final String TAG = FaceDetectionView.class.getName();
    private TextureView mPreview;
    private ViewfinderView mViewFinder;
    private CameraHolder mCameraHolder;
    private CameraHolder.CameraType mType;
    private boolean hasAspectRatio = false;
    private double aspect_ratio = -1;
    private RectF mDetectionArea = new RectF();
    private FaceDetectorCallback mCallback;
    private Camera.Face bestFace = null;
    private Camera.Face lastBestFace = null;
    private Matrix scalingMatrix = new Matrix();
    private FaceMatcher mMatcher;

    //TODO: debug
    private FaceOverlayView overlay;

    public FaceDetectionView(Context context) {
        super(context);
        init();
    }

    public FaceDetectionView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public FaceDetectionView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        if (isInEditMode())
            return;
        mViewFinder = new ViewfinderView(getContext());
        addView(mViewFinder);
        mViewFinder.init(getContext().getResources().getColor(R.color.viewfinder_color));

        mCameraHolder = new OldCameraHolder();
        mCameraHolder.setCallback(cameraCallback);

        mMatcher = new FaceMatcher(scalingMatrix);
        mMatcher.setMatchingArea(mDetectionArea);

        //TODO: Has to be removed
        if (DEBUG_MODE) {
            overlay = new FaceOverlayView(getContext(), scalingMatrix);
            overlay.setAreaRect(mDetectionArea);
            addView(overlay);
        }
    }

    public void setCallback(FaceDetectorCallback callback) {
        this.mCallback = callback;
    }

    public void start(CameraHolder.CameraType type) {
        this.mType = type;
        mPreview = new TextureView(getContext());
        mPreview.setSurfaceTextureListener(previewCallback);
        addView(mPreview, 0);
    }

    public void stop() {
        mCameraHolder.stop();
        removeView(mPreview);
    }

    private void startCamera() {
        if (mType == CameraHolder.CameraType.FRONT_FACING_CAMERA)
            mCameraHolder.setStartCameraAfterPhotoTaken(true);
        mCameraHolder.start(mPreview.getSurfaceTexture(), mType, getWidth(), getHeight());
    }

    public void takePhoto() {
        lastBestFace = bestFace;
        mCameraHolder.takePhoto();
    }

    private TextureView.SurfaceTextureListener previewCallback = new TextureView.SurfaceTextureListener() {
        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
            startCamera();
        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
            mCameraHolder.stop();
            return true;
        }

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture surface) {/*empty*/}

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {/*empty*/}
    };

    private CameraHolder.HolderCallback cameraCallback = new CameraHolder.HolderCallback() {
        @Override
        public void onStarted() {
            if (mCallback != null)
                mCallback.onDetectionStarted();
        }

        @Override
        public void onStopped() {
            if (mCallback != null)
                mCallback.onDetectionStopped();
        }

        @Override
        public void onPictureTaken(byte[] data) {
            if (mCallback == null)
                return;

            PhotoDecodingWorker worker = new PhotoDecodingWorker(mType, mDetectionArea, getMeasuredWidth(), getMeasuredHeight(), lastBestFace, new PhotoDecodingWorker.DecodingCallback() {
                @Override
                public void onDecoded(Bitmap bmp) {
                    mCallback.onPhotoTaken(bmp);
                }
            });
            worker.execute(data);
        }

        @Override
        public void onFaceDetection(List<Camera.Face> faces) {
            if (faces.size() > 0)
                bestFace = mMatcher.getTheBestFace((java.util.ArrayList<Camera.Face>) faces);
            else
                bestFace = null;

            if (mCallback != null)
                mCallback.onFaceDetection(faces.size() > 0);

            //TODO: Has to be removed
            if (DEBUG_MODE) {
                overlay.setFaces(faces);
                overlay.setBestFace(bestFace);
            }
        }

        @Override
        public void onError(CameraException e) {
            if (mCallback != null)
                mCallback.onError(e);
        }

        @Override
        public void onPreviewRatioCalculated(int width, int height) {
            hasAspectRatio = true;
            aspect_ratio = ((double) width / (double) height);
            requestLayout();
        }
    };

    public void setDetectionArea(RectF detectionArea) {
        mDetectionArea.set(detectionArea);
        mViewFinder.setFinderRect(detectionArea);
    }

    @Override
    protected void onMeasure(int widthSpec, int heightSpec) {
        if (!this.hasAspectRatio) {
            super.onMeasure(widthSpec, heightSpec);
            return;
        }
        int previewWidth = MeasureSpec.getSize(widthSpec);
        int previewHeight = MeasureSpec.getSize(heightSpec);

        // Get the padding of the border background.
        int hPadding = getPaddingLeft() + getPaddingRight();
        int vPadding = getPaddingTop() + getPaddingBottom();

        // Resize the preview frame with correct aspect ratio.
        previewWidth -= hPadding;
        previewHeight -= vPadding;

        boolean widthLonger = previewWidth > previewHeight;
        int longSide = (widthLonger ? previewWidth : previewHeight);
        int shortSide = (widthLonger ? previewHeight : previewWidth);
        if (longSide > shortSide * aspect_ratio) {
            longSide = (int) ((double) shortSide * aspect_ratio);
        } else {
            shortSide = (int) ((double) longSide / aspect_ratio);
        }
        if (widthLonger) {
            previewWidth = longSide;
            previewHeight = shortSide;
        } else {
            previewWidth = shortSide;
            previewHeight = longSide;
        }

        // Add the padding of the border.
        previewWidth += hPadding;
        previewHeight += vPadding;

        // Ask children to follow the new preview dimension.
        super.onMeasure(MeasureSpec.makeMeasureSpec(previewWidth, MeasureSpec.EXACTLY), MeasureSpec.makeMeasureSpec(previewHeight, MeasureSpec.EXACTLY));
        prepareFaceScalingMatrix(scalingMatrix, mType == CameraHolder.CameraType.FRONT_FACING_CAMERA, 90, getMeasuredWidth(), getMeasuredHeight());
    }

    private static void prepareFaceScalingMatrix(Matrix matrix, boolean mirror, int displayOrientation, int containerWidth, int containerHeight) {
        matrix.setScale(mirror ? -1 : 1, 1);
        matrix.postRotate(displayOrientation);
        matrix.postScale(containerWidth / 2000f, containerHeight / 2000f);
        matrix.postTranslate(containerWidth / 2f, containerHeight / 2f);
    }

    public interface FaceDetectorCallback {

        void onFaceDetection(boolean detected);

        void onDetectionStarted();

        void onDetectionStopped();

        void onPhotoTaken(Bitmap bmp);

        void onError(CameraException e);
    }


}
