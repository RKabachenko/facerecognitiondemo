package mx.camera.camera;

import android.graphics.SurfaceTexture;
import android.hardware.Camera;

import java.util.List;

public abstract class CameraHolder {
    public static final int PREVIEW_ORIENTATION = 90;
    protected HolderCallback mCallback;
    protected boolean startCameraAfterPhotoTaken = false;


    public void setCallback(HolderCallback callback) {
        this.mCallback = callback;
    }

    public static enum CameraType {
        FRONT_FACING_CAMERA, BACK_FACING_CAMERA
    }

    public void setStartCameraAfterPhotoTaken(boolean startCameraAfterPhotoTaken) {
        this.startCameraAfterPhotoTaken = startCameraAfterPhotoTaken;
    }

    public abstract void start(SurfaceTexture surface, CameraType type, int previewWidth, int previewHeight);

    public abstract void stop();

    public abstract void takePhoto();

    public abstract void startPreview();

    public abstract boolean isRunning();

    public static interface HolderCallback {
        void onStarted();

        void onStopped();

        void onPictureTaken(byte[] data);

        void onFaceDetection(List<Camera.Face> faces);

        void onError(CameraException e);

        void onPreviewRatioCalculated(int width, int height);
    }
}
