package mx.camera.camera;

import android.graphics.SurfaceTexture;

//TODO: Has to be implemented for android.hardware.camera2
public class LollipopCameraHolder extends CameraHolder {


    @Override
    public void start(SurfaceTexture surface, CameraType type, int previewWidth, int previewHeight) {

    }

    @Override
    public void stop() {

    }

    @Override
    public void takePhoto() {

    }

    @Override
    public void startPreview() {

    }

    @Override
    public boolean isRunning() {
        return false;
    }
}
