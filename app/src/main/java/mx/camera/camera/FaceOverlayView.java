package mx.camera.camera;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.hardware.Camera;
import android.view.View;

import java.util.List;

/**
 * Temporary class for debug face detection. Will be removed in future
 */
public class FaceOverlayView extends View {

    private Paint mPaint;
    private Paint mBestFacePaint;
    private Paint mAreaPaint;
    private RectF mAreaRect;
    private Paint mTextPaint;
    private List<Camera.Face> mFaces;
    private Camera.Face bestFace;
    private Matrix matrix;
    RectF rectF = new RectF();

    public FaceOverlayView(Context context, Matrix matrix) {
        super(context);
        this.matrix = matrix;
        initialize();
    }

    private void initialize() {
        setWillNotDraw(false);
        mPaint = getPaint(Color.GREEN, 3);
        mBestFacePaint = getPaint(Color.RED, 3);
        mAreaPaint = getPaint(Color.BLUE, 3);

        mTextPaint = new Paint();
        mTextPaint.setAntiAlias(true);
        mTextPaint.setDither(true);
        mTextPaint.setTextSize(16);
        mTextPaint.setColor(Color.RED);
        mTextPaint.setStyle(Paint.Style.FILL);
    }

    private Paint getPaint(int color, int strokeWidth) {
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setDither(true);
        paint.setColor(color);
        paint.setAlpha(180);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(strokeWidth);
        return paint;
    }

    public void setAreaRect(RectF rect) {
        mAreaRect = rect;
    }

    public void setFaces(List<Camera.Face> faces) {
        mFaces = faces;
    }

    public void setBestFace(Camera.Face face) {
        bestFace = face;
    }

    //Can be optimized, but it doesn't matter. This class is temporary
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if ((mFaces != null && mFaces.size() > 0) || mAreaPaint != null) {

            canvas.save();
            if (mAreaRect != null) {
                canvas.drawRect(mAreaRect, mAreaPaint);
            }

            if (mFaces != null && mFaces.size() > 0) {
                for (Camera.Face face : mFaces) {
                    rectF.set(face.rect);
                    matrix.mapRect(rectF);
                    canvas.drawRect(rectF, face.equals(bestFace) ? mBestFacePaint : mPaint);
                    canvas.drawText("Score " + face.score, rectF.right, rectF.top, mTextPaint);
                }
            }
            canvas.restore();
        }
        invalidate();
    }


}
