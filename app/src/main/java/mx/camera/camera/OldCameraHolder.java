package mx.camera.camera;

import android.graphics.Point;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class OldCameraHolder extends CameraHolder implements Camera.PictureCallback {

    private static final boolean DEBUG = true;
    private static final String TAG = OldCameraHolder.class.getName();
    private static final int OPTIMAL_PICTURE_SIZE = 1200;
    private Camera mCamera;
    private boolean running = false;

    public OldCameraHolder() {/*empty*/}

    public boolean isRunning() {
        return running;
    }

    @Override
    public void start(SurfaceTexture surface, CameraType type, int previewWidth, int previewHeight) {
        openCamera(surface, type, previewWidth, previewHeight);
    }

    public void stop() {
        running = false;
        if (mCallback != null)
            mCallback.onStopped();
        closeCamera();
    }

    public void takePhoto() {
        mCamera.takePicture(null, null, this);
    }

    private void openCamera(SurfaceTexture surface, CameraType type, int previewWidth, int previewHeight) {
        new CameraOpeningWorker(surface, type, previewWidth, previewHeight).execute();
    }

    private class CameraOpeningWorker extends AsyncTask<Void, Void, Camera> {
        private CameraType type;
        private int cameraId;
        private SurfaceTexture surface;
        private int previewWidth, previewHeight;

        public CameraOpeningWorker(SurfaceTexture surface, CameraType type, int previewWidth, int previewHeight) {
            this.surface = surface;
            this.type = type;
            this.previewWidth = previewWidth;
            this.previewHeight = previewHeight;
        }

        @Override
        protected Camera doInBackground(Void... params) {
            cameraId = getCameraId(type);
            return Camera.open(cameraId);
        }

        @Override
        protected void onPostExecute(Camera camera) {
            if (camera != null) {
                setCameraOrientation(cameraId, camera);
                camera.setErrorCallback(cameraErrorCallback);
                camera.setFaceDetectionListener(faceDetectionListener);
                try {
                    camera.setPreviewTexture(surface);
                } catch (IOException e) {
                    if (mCallback != null) {
                        mCallback.onError(new CameraException(e.getMessage(), 0));
                        return;
                    }

                }

                configCamera(camera, previewWidth, previewHeight);

                mCamera = camera;
                startPreview();
                if (!running && mCallback != null) {
                    mCallback.onStarted();
                    running = true;
                }
            } else {
                mCallback.onError(new CameraException("Camera is not available", -1));
                return;
            }
            super.onPostExecute(camera);
        }

        private void setCameraOrientation(int cameraId, Camera camera) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(cameraId, info);
            int result;

            if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                result = info.orientation % 360;
                result = (360 - result) % 360;
            } else {
                result = (info.orientation + 360) % 360;
            }
            camera.setDisplayOrientation(result);
        }

        private void configCamera(Camera camera, int previewWidth, int previewHeight) {
            Camera.Parameters parameters = camera.getParameters();

            Camera.Size pictureSize = getOptimalPictureSize(parameters.getSupportedPictureSizes());
            int pictureWidth = pictureSize.width;
            int pictureHeight = pictureSize.height;
            parameters.setPictureSize(pictureWidth, pictureHeight);
            camera.setParameters(parameters);
            parameters = camera.getParameters();

            Camera.Size previewSize = getOptimalPreviewSize(parameters.getSupportedPreviewSizes(), previewWidth, previewHeight, pictureWidth, pictureHeight);
            parameters.setPreviewSize(previewSize.width, previewSize.height);

            if (mCallback != null)
                mCallback.onPreviewRatioCalculated(previewSize.width, previewSize.height);

            List<String> focusModes = parameters.getSupportedFocusModes();
            if (focusModes != null && focusModes.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE))
                parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);

            List<String> flashModes = parameters.getSupportedFlashModes();
            if (flashModes != null && flashModes.contains(Camera.Parameters.FLASH_MODE_AUTO))
                parameters.setFlashMode(Camera.Parameters.FLASH_MODE_AUTO);

            parameters.setJpegQuality(80);
            camera.setParameters(parameters);

        }
    }

    public void startPreview() {
        if (mCamera != null) {
            mCamera.startPreview();

            if (mCamera.getParameters().getMaxNumDetectedFaces() > 0) {
                mCamera.startFaceDetection();
            }
        }
    }

    private void closeCamera() {
        if (mCamera != null) {
            mCamera.stopPreview();
            mCamera.setPreviewCallback(null);
            mCamera.setFaceDetectionListener(null);
            mCamera.setErrorCallback(null);
            mCamera.release();
            mCamera = null;
        }
    }

    private int getCameraId(CameraType type) {
        int camerasCount = Camera.getNumberOfCameras();
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        for (int i = 0; i < camerasCount; i++) {
            Camera.getCameraInfo(i, cameraInfo);
            if ((type == CameraType.FRONT_FACING_CAMERA && cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) ||
                    (type == CameraType.BACK_FACING_CAMERA && cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_BACK)) {
                return i;
            }
        }
        return -1;
    }


    @Override
    public void onPictureTaken(byte[] data, Camera camera) {
        if (startCameraAfterPhotoTaken) {
            mCamera.startPreview();
            try {
                mCamera.startFaceDetection();
            } catch (RuntimeException ignore) {/*empty*/}
        } else {
            closeCamera();
        }

        if (mCallback == null)
            return;
        mCallback.onPictureTaken(data);
    }

    private static Camera.Size getOptimalPictureSize(List<Camera.Size> supportedSizes) {
        int min = Integer.MAX_VALUE;
        Camera.Size bestSize = null;

        for (Camera.Size size : supportedSizes) {
            final int diff = Math.abs(size.width - OPTIMAL_PICTURE_SIZE);
            if (diff < min) {
                min = diff;
                bestSize = size;
            }
        }
        if (bestSize != null)
            Log.d(TAG, "" + bestSize.width + "x" + bestSize.height);
        return bestSize;
    }

    public Camera.Size getOptimalPreviewSize(List<Camera.Size> sizes, int displayWidth, int displayHeight, int pictureWidth, int pictureHeight) {
        final double ASPECT_TOLERANCE = 0.05;
        if (sizes == null)
            return null;

        Camera.Size optimalSize = null;
        double minDiff = Double.MAX_VALUE;
        Point displaySize = new Point(displayWidth, displayHeight);
        Point pictureSize = new Point(pictureWidth, pictureHeight);
        double targetRatio = getTargetRatioForPreview(displaySize, pictureSize);
        int targetHeight = Math.min(displaySize.y, displaySize.x);
        if (targetHeight <= 0) {
            targetHeight = displaySize.y;
        }
        // Try to find an size match aspect ratio and size
        for (Camera.Size size : sizes) {
//            if (DEBUG)
//                Log.d(TAG, "    supported preview size: " + size.width + ", " + size.height);
            double ratio = (double) size.width / size.height;
            if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE)
                continue;
            if (Math.abs(size.height - targetHeight) < minDiff) {
                optimalSize = size;
                minDiff = Math.abs(size.height - targetHeight);
            }
        }
        if (optimalSize == null) {
            // can't find match for aspect ratio, so find closest one
            if (DEBUG)
                Log.d(TAG, "no preview size matches the aspect ratio");
            optimalSize = getClosestSize(sizes, targetRatio);
        }
        if (DEBUG) {
            Log.d(TAG, "chose optimalSize: " + optimalSize.width + " x " + optimalSize.height);
            Log.d(TAG, "optimalSize ratio: " + ((double) optimalSize.width / optimalSize.height));
        }
        if (DEBUG)
            Log.d(TAG, "preview_size: " + optimalSize.width + "x" + optimalSize.height);
        return optimalSize;
    }

    public double getTargetRatioForPreview(Point displaySize, Point pictureSize) {
        if (DEBUG)
            Log.d(TAG, "picture_size: " + pictureSize.x + " x " + pictureSize.y);
        return ((double) pictureSize.x) / (double) pictureSize.y;
    }

    public Camera.Size getClosestSize(List<Camera.Size> sizes, double targetRatio) {
        if (DEBUG)
            Log.d(TAG, "getClosestSize()");
        Camera.Size optimalSize = null;
        double minDiff = Double.MAX_VALUE;
        for (Camera.Size size : sizes) {
            double ratio = (double) size.width / size.height;
            if (Math.abs(ratio - targetRatio) < minDiff) {
                optimalSize = size;
                minDiff = Math.abs(ratio - targetRatio);
            }
        }
        return optimalSize;
    }


    private Camera.ErrorCallback cameraErrorCallback = new Camera.ErrorCallback() {
        @Override
        public void onError(int error, Camera camera) {
            if (mCallback != null)
                mCallback.onError(new CameraException("Camera internal error", error));
        }
    };

    private Camera.FaceDetectionListener faceDetectionListener = new Camera.FaceDetectionListener() {
        @Override
        public void onFaceDetection(Camera.Face[] faces, Camera camera) {
            if (mCallback != null)
                mCallback.onFaceDetection(new ArrayList<Camera.Face>(Arrays.asList(faces)));
        }
    };


}
