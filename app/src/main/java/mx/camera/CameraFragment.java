package mx.camera;


import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.Toast;

import com.privatas.facerecognitiondemo.R;

import mx.camera.camera.CameraException;
import mx.camera.camera.CameraHolder;
import mx.camera.camera.FaceDetectionView;


public class CameraFragment extends Fragment {

    public static CameraFragment newInstance() {
        return new CameraFragment();
    }

    private FaceDetectionView mFaceDetector;
    private long time = 0;
    private boolean started = false;
    private View progressBar;
    private ImageView imgPreview;
    private Rect detectionArea = new Rect();

    public CameraFragment() {/*empty*/}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_camera, container, false);
        mFaceDetector = (FaceDetectionView) view.findViewById(R.id.detector);
        mFaceDetector.setCallback(callback);
        mFaceDetector.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!started)
                    mFaceDetector.start(CameraHolder.CameraType.BACK_FACING_CAMERA);
                else
                    mFaceDetector.stop();
                time = System.currentTimeMillis();
                started = !started;
            }
        });

        progressBar = view.findViewById(R.id.progressBar);

        View btnShoot = view.findViewById(R.id.btnShoot);
        btnShoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFaceDetector.takePhoto();
            }
        });

        imgPreview = (ImageView) view.findViewById(R.id.imgPreview);
        imgPreview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgPreview.setVisibility(View.GONE);
            }
        });

        final ViewTreeObserver vto = view.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int[] location = new int[2];
                progressBar.getLocationOnScreen(location);
                int left = location[0];
                int top = location[1];
                int right = left + progressBar.getMeasuredWidth();
                int bottom = top + progressBar.getMeasuredHeight();
                detectionArea.set(left, top, right, bottom);
                RectF rect = new RectF(detectionArea);
                mFaceDetector.setDetectionArea(rect);
            }
        });
        return view;
    }

    private FaceDetectionView.FaceDetectorCallback callback = new FaceDetectionView.FaceDetectorCallback() {
        @Override
        public void onFaceDetection(boolean detected) {

        }

        @Override
        public void onDetectionStarted() {
            Log.d("Camera Starting", "Started:" + (System.currentTimeMillis() - time / 1000d) + "sec");
        }

        @Override
        public void onDetectionStopped() {
            Log.d("Camera Starting", "Stopping:" + (System.currentTimeMillis() - time / 1000d) + "sec");
        }

        @Override
        public void onPhotoTaken(Bitmap bmp) {
            imgPreview.setImageBitmap(bmp);
            imgPreview.setVisibility(View.VISIBLE);
        }

        @Override
        public void onError(CameraException e) {
            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    };


}
