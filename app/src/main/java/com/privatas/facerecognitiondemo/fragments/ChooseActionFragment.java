package com.privatas.facerecognitiondemo.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.privatas.facerecognitiondemo.R;

public class ChooseActionFragment extends Fragment {
    private OnActionChooserListener mListener;
    private ActionChooserDataSource mDataSource;

    public ChooseActionFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_choose_action, container, false);

        Button scanFaceButton = (Button) view.findViewById(R.id.scan_face_button);
        Button registerButton = (Button) view.findViewById(R.id.registration_button);

        if (mDataSource != null) {
            scanFaceButton.setEnabled(mDataSource.isThereLoggedIndUser());
        }
        scanFaceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.scanFaceClicked();
                }
            }
        });


        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.registerClicked();
                }
            }
        });


        return view;
    }

    public interface OnActionChooserListener {
        public void scanFaceClicked();
        public void registerClicked();
        public void editProfileClicked();
    }

    public interface ActionChooserDataSource {
        public boolean isThereRegisteredUser();
        public boolean isThereLoggedIndUser();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnActionChooserListener) activity;
        } catch (ClassCastException e) {
            mListener = null;
        }

        try {
            mDataSource = (ActionChooserDataSource) activity;
        } catch (ClassCastException e) {
            mDataSource = null;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mDataSource = null;
        mListener = null;
    }
}
