package com.privatas.facerecognitiondemo.fragments;


import android.app.Activity;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.Point;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;

import com.privatas.facerecognitiondemo.R;

import java.util.List;

import static com.privatas.facerecognitiondemo.fragments.EditProfileFragment.decodeArray;

public class ScanFaceFragment extends Fragment {

    private Preview mPreview;
    Camera mCamera;
    FrameLayout cameraContainer;
    int mNumberOfCameras;
    int mCurrentCamera;  // Camera ID currently chosen
    int mCameraCurrentlyLocked;  // Camera ID that's actually acquired
    private static final int OPTIMAL_PICTURE_SIZE = 1200;
    // The first rear facing camera
    int mDefaultCameraId;
    private OnPhotoTakenListener mListener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Create a container that will hold a SurfaceView for camera previews
        // Find the total number of cameras available
        mNumberOfCameras = Camera.getNumberOfCameras();

        // Find the ID of the rear-facing ("default") camera
        CameraInfo cameraInfo = new CameraInfo();
        for (int i = 0; i < mNumberOfCameras; i++) {
            Camera.getCameraInfo(i, cameraInfo);
            if (cameraInfo.facing == CameraInfo.CAMERA_FACING_BACK) {
                mCurrentCamera = mDefaultCameraId = i;
            }
        }
        setHasOptionsMenu(mNumberOfCameras > 1);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_scan_face, container, false);

        cameraContainer = (FrameLayout) view.findViewById(R.id.camera_container);

        Button takePhotoButton = (Button) view.findViewById(R.id.take_photo_button);
        takePhotoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCamera != null) {
                    mCamera.takePicture(null, null, new Camera.PictureCallback() {
                        @Override
                        public void onPictureTaken(byte[] data, Camera camera) {
                            if (mListener != null) {
                                if (data != null) {
                                    Bitmap bm = decodeArray(data, 500, 500);
                                    int originalWidth = bm.getWidth();
                                    int originalHeight = bm.getHeight();

                                    if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                                        // Notice that width and height are reversed

//                                        Bitmap scaled = Bitmap.createScaledBitmap(bm, originalHeight, originalWidth, true);
                                        Bitmap scaled = bm;
                                        int w = scaled.getWidth();
                                        int h = scaled.getHeight();
                                        // Setting post rotate to 90
                                        Matrix mtx = new Matrix();
                                        mtx.postRotate(90);
                                        // Rotating Bitmap
                                        bm = Bitmap.createBitmap(scaled, 0, 0, w, h, mtx, true);
                                    } else {// LANDSCAPE MODE
                                        //No need to reverse width and height
                                        Bitmap scaled = Bitmap.createScaledBitmap(bm, originalWidth, originalHeight, true);
                                        bm = scaled;
                                    }

                                    MediaStore.Images.Media.insertImage(getActivity().getContentResolver(), bm, "FaceRecognitionDemoImage", "FaceRecognitionDemoImageDescription");
                                    mListener.photoTaken(bm);
                                }
                            }
                        }
                    });
                }
            }
        });
        return view;
    }


    private int[] configCamera(Camera camera) {
        Camera.Parameters parameters = camera.getParameters();

        Camera.Size pictureSize = getOptimalPictureSize(parameters.getSupportedPictureSizes());
        int pictureWidth = pictureSize.width;
        int pictureHeight = pictureSize.height;
        parameters.setPictureSize(pictureWidth, pictureHeight);
        camera.setParameters(parameters);
        parameters = camera.getParameters();

        Camera.Size previewSize = getOptimalPreviewSize(parameters.getSupportedPreviewSizes(), mPreview.getWidth(), mPreview.getHeight(), pictureWidth, pictureHeight);
        parameters.setPreviewSize(previewSize.width, previewSize.height);

        List<String> focusModes = parameters.getSupportedFocusModes();
        if (focusModes != null && focusModes.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) {
            parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
        }

        List<String> flashModes = parameters.getSupportedFlashModes();
//        if (flashModes != null && flashModes.contains(Camera.Parameters.FLASH_MODE_TORCH)) {
//            parameters.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
//        } else if (flashModes != null && flashModes.contains(Camera.Parameters.FLASH_MODE_ON)) {
//            parameters.setFlashMode(Camera.Parameters.FLASH_MODE_ON);
//        }

        if (flashModes != null && flashModes.contains(Camera.Parameters.FLASH_MODE_AUTO)) {
            parameters.setFlashMode(Camera.Parameters.FLASH_MODE_AUTO);
        }

        parameters.setJpegQuality(80);
        camera.setParameters(parameters);
        return new int[]{previewSize.width, previewSize.height};
    }

    private static Camera.Size getOptimalPictureSize(List<Camera.Size> supportedSizes) {
        int min = Integer.MAX_VALUE;
        Camera.Size bestSize = null;

        for (Camera.Size size : supportedSizes) {
            final int diff = Math.abs(size.width - OPTIMAL_PICTURE_SIZE);
            if (diff < min) {
                min = diff;
                bestSize = size;
            }
        }
        return bestSize;
    }

    public Camera.Size getOptimalPreviewSize(List<Camera.Size> sizes, int displayWidth, int displayHeight, int pictureWidth, int pictureHeight) {
        final double ASPECT_TOLERANCE = 0.05;
        if (sizes == null)
            return null;

        Camera.Size optimalSize = null;
        double minDiff = Double.MAX_VALUE;
        Point displaySize = new Point(displayWidth, displayHeight);
        Point pictureSize = new Point(pictureWidth, pictureHeight);
        double targetRatio = getTargetRatioForPreview(pictureSize);
        int targetHeight = Math.min(displaySize.y, displaySize.x);
        if (targetHeight <= 0) {
            targetHeight = displaySize.y;
        }
        // Try to find an size match aspect ratio and size
        for (Camera.Size size : sizes) {
            double ratio = (double) size.width / size.height;
            if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE)
                continue;
            if (Math.abs(size.height - targetHeight) < minDiff) {
                optimalSize = size;
                minDiff = Math.abs(size.height - targetHeight);
            }
        }
        if (optimalSize == null) {
            // Can't find match for aspect ratio, so find closest one
            optimalSize = getClosestSize(sizes, targetRatio);
        }
        return optimalSize;
    }

    public double getTargetRatioForPreview(Point pictureSize) {
        return ((double) pictureSize.x) / (double) pictureSize.y;
    }

    public Camera.Size getClosestSize(List<Camera.Size> sizes, double targetRatio) {
        Camera.Size optimalSize = null;
        double minDiff = Double.MAX_VALUE;
        for (Camera.Size size : sizes) {
            double ratio = (double) size.width / size.height;
            if (Math.abs(ratio - targetRatio) < minDiff) {
                optimalSize = size;
                minDiff = Math.abs(ratio - targetRatio);
            }
        }
        return optimalSize;
    }

    @Override
    public void onResume() {
        super.onResume();

        // Use mCurrentCamera to select the camera desired to safely restore
        // the fragment after the camera has been changed
        mPreview = new Preview(this.getActivity());
        cameraContainer.addView(mPreview);

        mCamera = Camera.open(mCurrentCamera);
        configCamera(mCamera);

        setCameraDisplayOrientation(getActivity(), mCurrentCamera, mCamera);
        mCameraCurrentlyLocked = mCurrentCamera;
        mPreview.setCamera(mCamera);
    }

    @Override
    public void onPause() {
        super.onPause();

        // Because the Camera object is a shared resource, it's very
        // important to release it when the activity is paused.
        if (mCamera != null) {
            mPreview.setCamera(null);
            mCamera.release();
            mCamera = null;
        }

        if (mPreview != null) {
            cameraContainer.removeAllViews();
            mPreview = null;
        }
    }

    public static void setCameraDisplayOrientation(Activity activity, int cameraId, android.hardware.Camera camera) {
        android.hardware.Camera.CameraInfo info = new android.hardware.Camera.CameraInfo();
        android.hardware.Camera.getCameraInfo(cameraId, info);
        int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
        int degrees = 0;
        switch (rotation) {
            case Surface.ROTATION_0:
                degrees = 0;
                break;
            case Surface.ROTATION_90:
                degrees = 90;
                break;
            case Surface.ROTATION_180:
                degrees = 180;
                break;
            case Surface.ROTATION_270:
                degrees = 270;
                break;
        }

        int result;
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360; // compensate the mirror
        } else { // back-facing
            result = (info.orientation - degrees + 360) % 360;
        }
        camera.setDisplayOrientation(result);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnPhotoTakenListener) activity;
        } catch (ClassCastException e) {
            mListener = null;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnPhotoTakenListener {
        public void photoTaken(Bitmap bitmap);
    }
}


