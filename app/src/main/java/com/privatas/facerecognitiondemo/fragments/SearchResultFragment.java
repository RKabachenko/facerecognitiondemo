package com.privatas.facerecognitiondemo.fragments;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.privatas.facerecognitiondemo.R;

import ly.tip.webservice.restmodels.TipUser;

public class SearchResultFragment extends Fragment {
    private OnSearchResultInteractionListener mListener;
    private SearchResultDataSource mDataSource;
    private TipUser foundUser;
    private Bitmap foundUserPhoto;
    private LinearLayout userDataHolder;
    private LinearLayout nothingFoundLinearLayout;
    private TextView tryAgainTextView;
    private boolean viewCreatedAndAssigned = false;

    ImageView profileCoverImage;
    TextView profileName, profileJob, profilePhone, profileEmail;

    public SearchResultFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_search_result, container, false);

        profileCoverImage = (ImageView)view.findViewById(R.id.profile_cover_photo);
        profileName = (TextView)view.findViewById(R.id.profile_name);
        profileJob = (TextView)view.findViewById(R.id.profile_job);
        profilePhone = (TextView)view.findViewById(R.id.profile_phone);
        profileEmail = (TextView)view.findViewById(R.id.profile_email);
        userDataHolder = (LinearLayout)view.findViewById(R.id.user_data_container);
        nothingFoundLinearLayout = (LinearLayout)view.findViewById(R.id.nothing_found_content);
        tryAgainTextView = (TextView)view.findViewById(R.id.try_again_text_view);

        tryAgainTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener!=null) {
                    mListener.retryLastSearch();
                }
            }
        });
        viewCreatedAndAssigned = true;

        setHasOptionsMenu(true);
        updateDataFromDataSource();
        updateUI();


        return view;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        viewCreatedAndAssigned = false;
        profileCoverImage = null;
        profileName = null;
        profileJob = null;
        profilePhone = null;
        profileEmail = null;
        userDataHolder = null;
        tryAgainTextView = null;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((ActionBarActivity)activity).getSupportActionBar().show();
        try {
            mListener = (OnSearchResultInteractionListener) activity;
        } catch (ClassCastException e) {
            mListener = null;
        }

        try {
            mDataSource = (SearchResultDataSource) activity;
        } catch (ClassCastException e) {
            mDataSource = null;
        }
        updateDataFromDataSource();
        updateUI();
    }

    @Override
    public void onDetach() {
        ((ActionBarActivity)getActivity()).getSupportActionBar().hide();
        mListener = null;
        super.onDetach();
    }

    public interface OnSearchResultInteractionListener {
        public void addToContacts(TipUser user);
        public void callToPhoneNumber(TipUser user);
        public void sendEmail(TipUser user);
        public void retryLastSearch();
    }

    public interface SearchResultDataSource {
        public TipUser getFoundUser();
        public Bitmap getFoundUserPhoto();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_search_result, menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mListener == null) {
            return false;
        }
        switch (item.getItemId()) {
            case R.id.action_add_to_contact:
                mListener.addToContacts(foundUser);
            case R.id.action_write_a_message:
                mListener.sendEmail(foundUser);
            default:
                break;
        }

        return true;
    }

    private void updateDataFromDataSource() {
        if (mDataSource == null) {
            return;
        }
        foundUser = mDataSource.getFoundUser();
        foundUserPhoto = mDataSource.getFoundUserPhoto();
    }

    private void updateUI() {
        if (!viewCreatedAndAssigned) {
            return;
        }

        if (foundUser == null) {
            setHasOptionsMenu(false);
            userDataHolder.setVisibility(View.GONE);
            nothingFoundLinearLayout.setVisibility(View.VISIBLE);
            profileName.setVisibility(View.GONE);
            profileCoverImage.setImageResource(R.drawable.nobody_sad);
        }
        else {
            setHasOptionsMenu(true);
            profileName.setVisibility(View.VISIBLE);
            userDataHolder.setVisibility(View.VISIBLE);
            nothingFoundLinearLayout.setVisibility(View.GONE);
            String[] parts = foundUser.lastName.split(";", -1);

            profileCoverImage.setImageBitmap(foundUserPhoto);
            profileName.setText(foundUser.firstName);
            profileJob.setText(foundUser.jobTitle);
            profilePhone.setText(parts[0] == null ? "" : parts[0]);
            profileEmail.setText(parts[1] == null ? "" : parts[1]);
        }
    }
}
