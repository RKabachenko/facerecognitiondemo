package com.privatas.facerecognitiondemo.fragments;

public class CameraException extends Exception {

    private String mMessage;
    private int mErrorCode;

    public CameraException(String message, int code) {
        mMessage = message;
        mErrorCode = code;
    }

    public String getMessage() {
        return mMessage;
    }

    public int getErrorCode() {
        return mErrorCode;
    }
}
