package com.privatas.facerecognitiondemo.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.melnykov.fab.FloatingActionButton;
import com.privatas.facerecognitiondemo.R;
import com.privatas.facerecognitiondemo.utils.Exif;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

import ly.tip.webservice.restmodels.User;

public class EditProfileFragment extends Fragment {
    static final int REQUEST_IMAGE_CAPTURE = 1;
    EditText name, jobTitle, phone, email;
    ImageView coverPhotoView;
    private Bitmap mCoverPhoto;
    private OnRegistrationAttempt mListener;
    private ProfileEditingDataSource mDataSource;
    private boolean viewLoadedAndAssigned = false;
    public boolean registration = false;

    public EditProfileFragment() {
        // Required empty public constructor
    }

    public static void copyFile(File src, File dst) throws IOException {
        FileChannel inChannel = new FileInputStream(src).getChannel();
        FileChannel outChannel = new FileOutputStream(dst).getChannel();
        try {
            inChannel.transferTo(0, inChannel.size(), outChannel);
        } finally {
            if (inChannel != null)
                inChannel.close();
            if (outChannel != null)
                outChannel.close();
        }
    }

    public static Bitmap decodeSampledBitmapFromFile(String path, int reqWidth, int reqHeight) { // BEST QUALITY MATCH

        //First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        // Calculate inSampleSize, Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        int inSampleSize = 1;

        if (height > reqHeight) {
            inSampleSize = Math.round((float) height / (float) reqHeight);
        }
        int expectedWidth = width / inSampleSize;

        if (expectedWidth > reqWidth) {
            //if(Math.round((float)width / (float)reqWidth) > inSampleSize) // If bigger SampSize..
            inSampleSize = Math.round((float) width / (float) reqWidth);
        }

        options.inSampleSize = inSampleSize;

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;

        return BitmapFactory.decodeFile(path, options);
    }

    void register() {
        if (mListener == null) {
            return;
        }

        User user = new User();

        if(name.length() > 0){
            user.firstName = name.getText().toString();
        }
        if (jobTitle.length() > 0) {
            user.jobTitle = jobTitle.getText().toString();
        }
        if (email.length() > 0) {
            user.emailAddress = email.getText().toString();
        }
        if (phone.length() > 0 && email.length() > 0) {
            user.lastName = phone.getText().toString() + ";" + email.getText().toString();
        }

        if (name.length() > 0 && jobTitle.length() > 0 && email.length() > 0 && phone.length() > 0 && mCoverPhoto != null) {
            mListener.tryToRegister(user, mCoverPhoto, registration);
        }
        else {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
            alertDialogBuilder.setTitle(R.string.app_name);
            TextView myMsg = new TextView(getActivity());
            myMsg.setText("All fields are necessary");
            myMsg.setTextSize(22);
            myMsg.setGravity(Gravity.CENTER);
            alertDialogBuilder.setView(myMsg);

            alertDialogBuilder.setNeutralButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialogBuilder.create().show();
        }

    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            File file = new File(Environment.getExternalStorageDirectory() + File.separator + "image.jpg");
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
            getActivity().startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {
            //Get our saved file into a bitmap object:
            File file = new File(Environment.getExternalStorageDirectory() + File.separator + "image.jpg");
            File newFile = new File(getActivity().getFilesDir(), "temp_photo");
            try {
                copyFile(file, newFile);
            } catch (IOException e) {
                e.printStackTrace();
            }

            mCoverPhoto = decodeFile(newFile.getAbsolutePath(), 500, 500);
            coverPhotoView.setImageBitmap(mCoverPhoto);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnRegistrationAttempt) activity;
        } catch (ClassCastException e) {
            mListener = null;
        }

        try {
            mDataSource = (ProfileEditingDataSource) activity;
        } catch (ClassCastException e) {
            mDataSource = null;
        }

        updateUI();
    }

    @Override
    public void onResume() {
        super.onResume();
        ((ActionBarActivity) getActivity()).getSupportActionBar().show();
    }

    @Override
    public void onPause() {
        ((ActionBarActivity) getActivity()).getSupportActionBar().hide();
        super.onPause();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_edit_profile, container, false);
        name = (EditText) view.findViewById(R.id.profile_name);
        jobTitle = (EditText) view.findViewById(R.id.profile_job);
        phone = (EditText) view.findViewById(R.id.profile_phone);
        email = (EditText) view.findViewById(R.id.profile_email);
        coverPhotoView = (ImageView) view.findViewById(R.id.profile_cover_photo);

        FloatingActionButton button = (FloatingActionButton) view.findViewById(R.id.fab);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dispatchTakePictureIntent();
            }
        });
        viewLoadedAndAssigned = true;
        updateUI();
        return view;
    }

    @Override
    public void onDetach() {
        mListener = null;
        mDataSource = null;
        super.onDetach();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_edit_profile, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_done:
                register();
            default:
                break;
        }
        return true;
    }

    public static Bitmap decodeFile(String path, int reqHeight, int reqWidth) {
        int orientation;
        try {
            if (path == null) {
                return null;
            }
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(path, options);

            final int height = options.outHeight;
            final int width = options.outWidth;
            options.inPreferredConfig = Bitmap.Config.RGB_565;
            int inSampleSize = 1;

            if (height > reqHeight) {
                inSampleSize = Math.round((float) height / (float) reqHeight);
            }
            int expectedWidth = width / inSampleSize;

            if (expectedWidth > reqWidth) {
                inSampleSize = Math.round((float) width / (float) reqWidth);
            }

            options.inSampleSize = inSampleSize;

            // Decode bitmap with inSampleSize set
            options.inJustDecodeBounds = false;

            Bitmap bm = BitmapFactory.decodeFile(path, options);


            ExifInterface exif = new ExifInterface(path);

            orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);

            Matrix m = new Matrix();

            if ((orientation == ExifInterface.ORIENTATION_ROTATE_180)) {
                m.postRotate(180);
                Log.e("in orientation", "" + orientation);
               return Bitmap.createBitmap(bm, 0, 0, bm.getWidth(),
                        bm.getHeight(), m, true);
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                m.postRotate(90);
                Log.e("in orientation", "" + orientation);
                return Bitmap.createBitmap(bm, 0, 0, bm.getWidth(),
                        bm.getHeight(), m, true);
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
                m.postRotate(270);
                Log.e("in orientation", "" + orientation);
                return Bitmap.createBitmap(bm, 0, 0, bm.getWidth(),
                        bm.getHeight(), m, true);
            }
            return bm;
        } catch (Exception e) {
            return null;
        }
    }

    public static Bitmap decodeArray(byte[] data, int reqHeight, int reqWidth) {
        int orientation;
        try {
            if (data == null) {
                return null;
            }
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeByteArray(data, 0, data.length, options);

            final int height = options.outHeight;
            final int width = options.outWidth;
            options.inPreferredConfig = Bitmap.Config.RGB_565;
            int inSampleSize = 1;

            if (height > reqHeight) {
                inSampleSize = Math.round((float) height / (float) reqHeight);
            }
            int expectedWidth = width / inSampleSize;

            if (expectedWidth > reqWidth) {
                inSampleSize = Math.round((float) width / (float) reqWidth);
            }

            options.inSampleSize = inSampleSize;

            // Decode bitmap with inSampleSize set
            options.inJustDecodeBounds = false;

            Bitmap bm = BitmapFactory.decodeByteArray(data, 0, data.length, options);




            orientation = Exif.getOrientation(data);

            Matrix m = new Matrix();

            if ((orientation == 180)) {
                m.postRotate(180);
                return Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), m, true);
            } else if (orientation == 90) {
                m.postRotate(90);
                return Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), m, true);
            } else if (orientation == 270) {
                m.postRotate(270);
                return Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), m, true);
            }

            return bm;
        } catch (Exception e) {
            return null;
        }
    }

    private void updateUI() {
        if (registration) {
            return;
        }

        User user = mDataSource.getAlreadyRegisteredUser();
        Bitmap userPhoto = mDataSource.getAlreadyRegisteredUserPhoto();

        if (!viewLoadedAndAssigned || user == null) {
            return;
        }

        String[] parts = user.lastName.split(";", -1);

        name.setText(user.firstName);
        jobTitle.setText(user.jobTitle);
        email.setText(parts[1]);
        phone.setText(parts[0]);

        if (userPhoto != null) {
            coverPhotoView.setImageBitmap(userPhoto);
            mCoverPhoto = userPhoto;
        }


    }

    public interface OnRegistrationAttempt {
        public void tryToRegister(User userModel, Bitmap coverPhoto, boolean registration);
    }


    public interface ProfileEditingDataSource {
        public User getAlreadyRegisteredUser();

        public Bitmap getAlreadyRegisteredUserPhoto();
    }
}
