package com.privatas.facerecognitiondemo.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.view.Window;
import android.widget.Toast;

import com.privatas.facerecognitiondemo.BuildConfig;
import com.privatas.facerecognitiondemo.R;
import com.privatas.facerecognitiondemo.fragments.ChooseActionFragment;
import com.privatas.facerecognitiondemo.fragments.EditProfileFragment;
import com.privatas.facerecognitiondemo.fragments.ScanFaceFragment;
import com.privatas.facerecognitiondemo.fragments.SearchResultFragment;
import com.privatas.facerecognitiondemo.fragments.SplashFragment;
import com.privatas.facerecognitiondemo.utils.LocalPersistence;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import ly.tip.webservice.WebAPIManager;
import ly.tip.webservice.restmodels.CardPaymentMethod;
import ly.tip.webservice.restmodels.PaymentMethod;
import ly.tip.webservice.restmodels.TipRequest;
import ly.tip.webservice.restmodels.TipUser;
import ly.tip.webservice.restmodels.User;
import ly.tip.webservice.restmodels.UserPhotosStatus;
import mx.camera.CameraFragment;

import static com.privatas.facerecognitiondemo.fragments.EditProfileFragment.copyFile;
import static com.privatas.facerecognitiondemo.fragments.EditProfileFragment.decodeFile;


public class MainActivity extends ActionBarActivity implements ChooseActionFragment.OnActionChooserListener, ScanFaceFragment.OnPhotoTakenListener, EditProfileFragment.OnRegistrationAttempt, EditProfileFragment.ProfileEditingDataSource, SearchResultFragment.SearchResultDataSource, SearchResultFragment.OnSearchResultInteractionListener, ChooseActionFragment.ActionChooserDataSource {
    private static final int MAX_TRY_COUNT = 6;
    static final int REQUEST_IMAGE_CAPTURE = 2;
    private static final String USER_PASSWORD = "123456Qa";
    private static final String USER_SERIALIZED = "saved_user";
    private static final String USER_PHOTO_SAVED = "saved_photo";


    private static final String FAKE_USER_EMAIL = "fake_user2@email.com";

    private static final ScheduledExecutorService worker = Executors.newSingleThreadScheduledExecutor();
    private static int triesCount = 0;
    /*Web service wrapper instance*/
    private WebAPIManager mSearchWebAPIManager;
    private WebAPIManager mWebAPIManager;

    /*Registered user part*/
    private User mRegisteredUser;
    private Bitmap mRegisteredUserPhoto;
    private boolean mUserSucessfullyLoggedIn;

    /*Search result part*/
    private TipUser mFoundUser;
    private Bitmap mFoundUserProfilePicture;
    private Bitmap bitmapForSearch;

    /*Progress*/
    private ProgressDialog mProgressDialog;

    private static boolean getStatus(boolean isPhotosSimilar, boolean isPhotosUnique, boolean isPhotosFailed) {
        boolean status = true;
        if (isPhotosFailed)
            status = false;
        else if (!isPhotosUnique)
            status = false;
        else if (!isPhotosSimilar)
            status = false;

        return status;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getSupportFragmentManager().popBackStack();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
        setContentView(R.layout.activity_main);
        transitionToFragment(new SplashFragment(), false);


        mWebAPIManager = new WebAPIManager();
        mSearchWebAPIManager = new WebAPIManager();

        final Runnable task = new Runnable() {
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        transitionToFragment(new ChooseActionFragment(), false);
                    }
                });
            }
        };

        mRegisteredUser = new User();
        mRegisteredUser.emailAddress = FAKE_USER_EMAIL;
        mRegisteredUser.password = USER_PASSWORD;

        loginFakeUser(mRegisteredUser, new Runnable() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        worker.schedule(task, 3, TimeUnit.SECONDS);
                    }
                });
            }
        }, null);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().hide();
    }


    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            File file = new File(Environment.getExternalStorageDirectory() + File.separator + "image.jpg");
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {
            //Get our saved file into a bitmap object:
            File file = new File(Environment.getExternalStorageDirectory() + File.separator + "image.jpg");
            File newFile = new File(getFilesDir(), "temp_photo");
            try {
                copyFile(file, newFile);
            } catch (IOException e) {
                e.printStackTrace();
            }

            Bitmap tempBitmap = decodeFile(newFile.getAbsolutePath(), 500, 500);
            photoTaken(tempBitmap);
        } else {
            List<Fragment> fragments = getSupportFragmentManager().getFragments();
            for (Fragment fragment : fragments) {
                if (fragment != null) {
                    fragment.onActivityResult(requestCode, resultCode, data);
                }
            }
        }
    }


    private void transitionToFragment(Fragment fragment, boolean addToBackStack) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN_MR1 && isDestroyed()) {
            return;
        }

        if (isFinishing()) {
            return;
        }

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragments_container, fragment);
        if (addToBackStack) {
            fragmentTransaction.addToBackStack(null);
        }
        fragmentTransaction.commitAllowingStateLoss();
    }

    private void unwindBackStack() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                getSupportFragmentManager().popBackStack();
            }
        });
    }

    @Override
    public void scanFaceClicked() {
        transitionToFragment(CameraFragment.newInstance(), true);
//        dispatchTakePictureIntent();
    }

    @Override
    public void registerClicked() {
        EditProfileFragment fragment = new EditProfileFragment();
        fragment.registration = true;
        transitionToFragment(fragment, true);
    }

    @Override
    public void editProfileClicked() {
        EditProfileFragment fragment = new EditProfileFragment();
        fragment.registration = false;
        transitionToFragment(fragment, true);
    }

    @Override
    public void photoTaken(Bitmap bitmap) {
        bitmapForSearch = bitmap;
        if (BuildConfig.DEBUG) {
            MediaStore.Images.Media.insertImage(this.getContentResolver(), bitmap, "FaceRecognitionDemoImage", "FaceRecognitionDemoImageDescription");
        }
        //bitmap = BitmapUtils.toGrayscale(bitmap);
        //MediaStore.Images.Media.insertImage(this.getContentResolver(), bitmap, "FaceRecognitionDemoImage", "FaceRecognitionDemoImageDescription");
        createTipRequest(bitmap, null, null);
    }


    private void tryToRegisterFakeUser(final Runnable callback) {
        final User fakeUser = new User();
        fakeUser.firstName = "Fake user";
        fakeUser.jobTitle = "Search worker";
        fakeUser.emailAddress = FAKE_USER_EMAIL;
        fakeUser.lastName = "fake user last name";
        fakeUser.zipCode = "12345";
        fakeUser.userType = User.UserType.TIP_RECEIVE_TIPS;
        fakeUser.password = USER_PASSWORD;
        fakeUser.termsAccepted = true;
        fakeUser.status = fakeUser.emailAddress;


        showProgressDialog(getString(R.string.cant_get_access));

        final Context context = this;

        final WebAPIManager.WebCallback<User> userWebCallback = new WebAPIManager.WebCallback<User>() {
            @Override
            public void onSuccess(User result) {
                Toast.makeText(context, getString(R.string.registration_successfull), Toast.LENGTH_LONG).show();
                saveUserToLocal(result);

                loginFakeUser(fakeUser, callback, new Runnable() {
                    @Override
                    public void run() {
                        showTryAgainDialog();
                    }
                });
            }

            @Override
            public void onError(Error error) {
                //TODO: notify user
                showTryAgainDialog();
                Toast.makeText(context, getString(R.string.registration_failed), Toast.LENGTH_LONG).show();
                dismissProgressDialog();
            }
        };

        mSearchWebAPIManager.createUser(fakeUser, null, userWebCallback);
    }

    private void showTryAgainDialog() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (!isFinishing()) {
                    new AlertDialog.Builder(MainActivity.this)
                            .setTitle(getString(R.string.app_name))
                            .setMessage("Can't get access to server")
                            .setCancelable(false)
                            .setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Runnable task = new Runnable() {
                                        public void run() {
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    transitionToFragment(new ChooseActionFragment(), false);
                                                }
                                            });
                                        }
                                    };

                                    mRegisteredUser = new User();
                                    mRegisteredUser.emailAddress = FAKE_USER_EMAIL;
                                    mRegisteredUser.password = USER_PASSWORD;
                                    loginFakeUser(mRegisteredUser, task, null);
                                    dialog.dismiss();
                                }
                            })
                            .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }).create().show();
                }
            }
        });
    }

    @Override
    public void tryToRegister(final User userModel, final Bitmap coverPhoto, boolean registration) {
        showProgressDialog(getString(R.string.registration_in_progress));

        final Context context = this;

        userModel.zipCode = "12345";
        userModel.userType = User.UserType.TIP_RECEIVE_TIPS;
        userModel.password = USER_PASSWORD;
        userModel.termsAccepted = true;
        userModel.status = userModel.emailAddress;


        final WebAPIManager.WebCallback<User> userWebCallback = new WebAPIManager.WebCallback<User>() {
            @Override
            public void onSuccess(User result) {
                mRegisteredUser = result;
                mRegisteredUserPhoto = coverPhoto;

                Toast.makeText(context, getString(R.string.registration_successfull), Toast.LENGTH_LONG).show();

                login(userModel, new Runnable() {
                    @Override
                    public void run() {
                        // Registering photo
                        ArrayList<Bitmap> photos = new ArrayList<>();
                        photos.add(coverPhoto);
                        mWebAPIManager.replaceUserPhotos(photos, new WebAPIManager.WebEmptyCallback() {
                            @Override
                            public void onSuccess() {
                                checkUserPhotoStatus(new PhotoStatusCallback() {
                                    @Override
                                    public void marched(boolean matched) {
                                        if (matched) {
                                            File file = new File(getFilesDir(), "temp_photo");
                                            File newFile = new File(getFilesDir(), USER_PHOTO_SAVED);
                                            try {
                                                copyFile(file, newFile);
                                            } catch (IOException e) {
                                                e.printStackTrace();
                                            }
                                            dismissProgressDialog();
                                            unwindBackStack();
                                        } else {
                                            Toast.makeText(context, getString(R.string.registering_user_photos_failed), Toast.LENGTH_LONG).show();
                                            dismissProgressDialog();
                                        }
                                    }
                                });
                            }

                            @Override
                            public void onError(Error error) {
                                dismissProgressDialog();
                                Toast.makeText(context, getString(R.string.registering_user_photos_failed) + "\n" + error.getErrorMessage(), Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                });
            }

            @Override
            public void onError(Error error) {
                Toast.makeText(context, getString(R.string.registration_failed) + "\n" + error.getErrorMessage(), Toast.LENGTH_LONG).show();
                final WebAPIManager.WebCallback<User> outerCallback = this;
                login(userModel, new Runnable() {
                    @Override
                    public void run() {
                        mWebAPIManager.updateUser(userModel, new WebAPIManager.WebCallback<User>() {
                            @Override
                            public void onSuccess(User result) {
                                outerCallback.onSuccess(result);
                            }

                            @Override
                            public void onError(Error error) {
                                Toast.makeText(context, getString(R.string.registration_failed), Toast.LENGTH_LONG).show();
                                dismissProgressDialog();
                            }
                        });
                    }
                });

                Toast.makeText(context, getString(R.string.registration_failed), Toast.LENGTH_LONG).show();
                dismissProgressDialog();
            }
        };


        createTipRequest(coverPhoto, new Runnable() {
            @Override
            public void run() {
                Toast.makeText(MainActivity.this, getString(R.string.photo_already_registered), Toast.LENGTH_LONG).show();
            }
        }, new Runnable() {
            @Override
            public void run() {
                mWebAPIManager.createUser(userModel, coverPhoto, userWebCallback);
            }
        });
    }

    private void saveUserToLocal(User userModel) {
        LocalPersistence.witeObjectToFile(this, userModel, USER_SERIALIZED);
    }


    private void showProgressDialog(final CharSequence message) {
        dismissProgressDialog();
        final Context context = this;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mProgressDialog = ProgressDialog.show(context, getString(R.string.app_name), message);
            }
        });
    }

    private void dismissProgressDialog() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mProgressDialog != null) {
                    mProgressDialog.dismiss();
                    mProgressDialog = null;
                }
            }
        });
    }

    private void login(User userModel, final Runnable callback) {
        if (mRegisteredUser == null) {
            return;
        }

        final Context context = this;

        mWebAPIManager.login(userModel.emailAddress, USER_PASSWORD, new WebAPIManager.WebCallback<Integer>() {
            private void finishOnError() {
                mUserSucessfullyLoggedIn = false;
                tryToRegisterFakeUser(callback);
            }

            @Override
            public void onSuccess(Integer result) {
                if (result == 200) {

                    getUpdatedUser();
                    mUserSucessfullyLoggedIn = true;
                    if (callback != null) {
                        callback.run();
                    }

                } else {
                    finishOnError();
                }
            }

            @Override
            public void onError(Error error) {
                finishOnError();
            }


        });

    }

    private void loginFakeUser(User userModel, final Runnable callback, final Runnable errorCallback) {
        if (mRegisteredUser == null) {
            return;
        }

        final Context context = this;

        mSearchWebAPIManager.login(userModel.emailAddress, USER_PASSWORD, new WebAPIManager.WebCallback<Integer>() {
            private void finishOnError() {
                if (errorCallback != null) {
                    errorCallback.run();
                }

                Toast.makeText(context, getString(R.string.login_failed), Toast.LENGTH_LONG).show();
                mUserSucessfullyLoggedIn = false;
                tryToRegisterFakeUser(callback);
            }

            @Override
            public void onSuccess(Integer result) {
                if (result == 200) {
                    Toast.makeText(context, getString(R.string.login_successful), Toast.LENGTH_LONG).show();

                    getUpdatedUser();
                    mUserSucessfullyLoggedIn = true;
                    if (callback != null) {
                        callback.run();
                    }

                } else {
                    finishOnError();
                }
            }

            @Override
            public void onError(Error error) {
                finishOnError();
            }
        });

    }

    private void createPaymentMethod() {
        CardPaymentMethod paymentMethod = new CardPaymentMethod();
        paymentMethod.cardholdersName = mRegisteredUser.firstName;
        paymentMethod.cardType = CardPaymentMethod.CardType.VISA;
        paymentMethod.cvv = "123";
        paymentMethod.digits = "4111111111111111";
        paymentMethod.zipCode = mRegisteredUser.zipCode;
        paymentMethod.expMonth = 12;
        paymentMethod.expYear = 2017;
        paymentMethod.mDefault = true;
        paymentMethod.name = "SomeCard";

        mSearchWebAPIManager.createPaymentMethod(paymentMethod, new WebAPIManager.WebCallback<PaymentMethod.Holder>() {
            @Override
            public void onSuccess(PaymentMethod.Holder result) {
                getUpdatedUser();
            }

            @Override
            public void onError(Error error) {
            }
        });
    }

    private void getUpdatedUser() {
        mSearchWebAPIManager.getUser(new WebAPIManager.WebCallback<User>() {
            @Override
            public void onSuccess(User result) {
                mRegisteredUser = result;
                saveUserToLocal(mRegisteredUser);
                if (mRegisteredUser.tipMethods.size() > 0) {
                    PaymentMethod defaultCreditCard = mRegisteredUser.tipMethods.get(0);
                    if (!defaultCreditCard.mDefault) {
                        defaultCreditCard.mDefault = true;
                        mSearchWebAPIManager.updatePaymentMethod(defaultCreditCard, new WebAPIManager.WebCallback<PaymentMethod>() {
                            @Override
                            public void onSuccess(PaymentMethod result) {
                                getUpdatedUser();
                            }

                            @Override
                            public void onError(Error error) {
                                getUpdatedUser();
                            }
                        });
                    }
                } else {
                    createPaymentMethod();
                }
            }

            @Override
            public void onError(Error error) {
                dismissProgressDialog();
            }
        });
    }

    void createTipRequest(Bitmap photo, final Runnable successCallback, final Runnable errorCallback) {
        mFoundUser = null;
        mFoundUserProfilePicture = null;

        if (mRegisteredUser == null) {
            return;
        }

        mRegisteredUser.makeCreditCardDefault();

        PaymentMethod defaultCreditCard = mRegisteredUser.defaultCreditCard();
        if (defaultCreditCard == null) {
            return;
        }

        showProgressDialog(getString(R.string.uploading_photo));
        final TipRequest tipRequest = new TipRequest();
        tipRequest.id = UUID.randomUUID().toString();
        tipRequest.amount = 1;
        tipRequest.latitude = 0;
        tipRequest.longitude = 0;

        tipRequest.paymentMethodId = defaultCreditCard.id;
        mSearchWebAPIManager.createTip(tipRequest, photo, new WebAPIManager.WebCallback<TipRequest>() {
            @Override
            public void onSuccess(TipRequest result) {
                triesCount = 0;
                dismissProgressDialog();
                checkTipStatus(tipRequest, successCallback, errorCallback);
            }

            @Override
            public void onError(Error error) {
                if (errorCallback != null) {
                    errorCallback.run();
                }
                Toast.makeText(MainActivity.this, getString(R.string.connection_error_message), Toast.LENGTH_LONG).show();
                dismissProgressDialog();
            }
        });
    }

    private void checkTipStatus(final TipRequest tipRequest, final Runnable successCallback, final Runnable errorCallback) {
        if (triesCount >= MAX_TRY_COUNT) {
            dismissProgressDialog();
            if (errorCallback != null) {
                errorCallback.run();
            } else {
                transitionToFragment(new SearchResultFragment(), true);
            }
            return;
        }
        if (mProgressDialog == null) {
            showProgressDialog(getString(R.string.photo_matching_in_progress));
        }
        mSearchWebAPIManager.getTipRequestDetails(tipRequest.id, new WebAPIManager.WebCallback<TipRequest>() {
            @Override
            public void onSuccess(final TipRequest result) {
                triesCount += 1;
                switch (result.status) {
                    case NEW:
                        worker.schedule(new Runnable() {
                            @Override
                            public void run() {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        checkTipStatus(tipRequest, successCallback, errorCallback);
                                    }
                                });
                            }
                        }, 5, TimeUnit.SECONDS);
                        break;
                    case MATCHED:
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                dismissProgressDialog();

                                if (successCallback != null) {
                                    successCallback.run();
                                } else {
                                    getRecipientData(result.recipientId);
                                }

                            }
                        });
                        break;
                    case CANCELED:
                    case FAILED:
                    case PAYED:
                        break;
                }
            }

            @Override
            public void onError(Error error) {
                if (errorCallback != null) {
                    errorCallback.run();
                }
                Toast.makeText(MainActivity.this, getString(R.string.connection_error_message), Toast.LENGTH_LONG).show();
                dismissProgressDialog();
            }
        });
    }

    private void getRecipientData(final String recipientId) {
        showProgressDialog(getString(R.string.loading_found_data));
        mSearchWebAPIManager.getUser(recipientId, new WebAPIManager.WebCallback<TipUser>() {
            @Override
            public void onSuccess(TipUser result) {
                mFoundUser = result;
                dismissProgressDialog();
                getUserPhoto(result);
            }

            @Override
            public void onError(Error error) {
                dismissProgressDialog();
            }
        });
    }

    private void getUserPhoto(TipUser user) {
        showProgressDialog(getString(R.string.loading_photo));
        mSearchWebAPIManager.getUserProfilePicture(user.profilePictureId, null, null, new WebAPIManager.WebCallback<Bitmap>() {
            @Override
            public void onSuccess(Bitmap result) {
                mFoundUserProfilePicture = result;
                dismissProgressDialog();
                transitionToFragment(new SearchResultFragment(), true);
            }

            @Override
            public void onError(Error error) {
                dismissProgressDialog();
            }
        });
    }

    @Override
    public User getAlreadyRegisteredUser() {
        return mRegisteredUser;
    }

    @Override
    public Bitmap getAlreadyRegisteredUserPhoto() {
        return mRegisteredUserPhoto;
    }

    @Override
    public TipUser getFoundUser() {
        return mFoundUser;
    }

    @Override
    public Bitmap getFoundUserPhoto() {
        return mFoundUserProfilePicture;
    }

    private void checkUserPhotoStatus(final PhotoStatusCallback callback) {
        if (callback == null) {
            return;
        }
        mWebAPIManager.getUserPhotoStatus(new WebAPIManager.WebCallback<List<UserPhotosStatus>>() {
            @Override
            public void onSuccess(List<UserPhotosStatus> result) {
                boolean photosReady = true;
                boolean photosSimilar = true;
                boolean photosUnique = true;
                boolean photosFailed = false;

                for (UserPhotosStatus status : result) {
                    if (status.unique.equals(UserPhotosStatus.PhotoStatus.NOT_PROCESSED))
                        photosReady = false;
                    if (status.unique.equals(UserPhotosStatus.PhotoStatus.PROCESSED_FAILURE) || status.similar.equals(UserPhotosStatus.PhotoStatus.PROCESSED_FAILURE))
                        photosFailed = true;
                    if (status.unique.equals(UserPhotosStatus.PhotoStatus.PROCESSED_FALSE))
                        photosUnique = false;
                    if (status.similar.equals(UserPhotosStatus.PhotoStatus.PROCESSED_FALSE))
                        photosSimilar = false;
                }
                if (!photosReady) {
                    worker.schedule(new Runnable() {
                        @Override
                        public void run() {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    checkUserPhotoStatus(callback);
                                }
                            });
                        }
                    }, 5, TimeUnit.SECONDS);
                    return;
                } else {
                    dismissProgressDialog();
                }

                final boolean status = getStatus(photosSimilar, photosUnique, photosFailed);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        dismissProgressDialog();
                        callback.marched(status);
                    }
                });
            }

            @Override
            public void onError(Error error) {
                dismissProgressDialog();
            }
        });
    }

    @Override
    public void addToContacts(TipUser user) {
        String[] parts = user.lastName.split(";", -1);

        Intent intent = new Intent(Intent.ACTION_INSERT);
        intent.setType(ContactsContract.Contacts.CONTENT_TYPE);

        intent.putExtra(ContactsContract.Intents.Insert.NAME, user.firstName);
        intent.putExtra(ContactsContract.Intents.Insert.PHONE, parts[0]);
        intent.putExtra(ContactsContract.Intents.Insert.EMAIL, parts[1]);
        intent.putExtra(ContactsContract.Intents.Insert.JOB_TITLE, user.jobTitle);
        startActivity(intent);
    }

    @Override
    public void callToPhoneNumber(TipUser user) {

    }

    @Override
    public void sendEmail(TipUser user) {
        final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
        emailIntent.setType("message/rfc822");
        String[] parts = user.lastName.split(";", -1);

        emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{parts[1]});

        startActivity(Intent.createChooser(emailIntent, getString(R.string.sending_email)));
    }

    @Override
    public void retryLastSearch() {
        unwindBackStack();
        scanFaceClicked();
    }

    @Override
    public boolean isThereRegisteredUser() {
        return mRegisteredUser != null;
    }

    @Override
    public boolean isThereLoggedIndUser() {
        return mRegisteredUser != null && mUserSucessfullyLoggedIn;
    }

    private interface PhotoStatusCallback {
        void marched(boolean matched);
    }
}
