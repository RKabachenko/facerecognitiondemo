package com.privatas.facerecognitiondemo.utils;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;

/**
 * Created by Serhiy.Krasovskyy on 18.03.2015.
 */
public class BitmapUtils {

    private final static int GREYSCALE_HIGH_LEVEL = 255;

    public static Bitmap toGrayscale(Bitmap bmpOriginal) {
        final int height = bmpOriginal.getHeight();
        final int width = bmpOriginal.getWidth();

        Bitmap bmpGrayscale = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
        final Canvas c = new Canvas(bmpGrayscale);
        final Paint paint = new Paint();
        final float[] mat = new float[]{
                0.3f, 0.59f, 0.11f, 0, 0,
                0.3f, 0.59f, 0.11f, 0, 0,
                0.3f, 0.59f, 0.11f, 0, 0,
                0, 0, 0, 1, 0,};
        final ColorMatrixColorFilter filter = new ColorMatrixColorFilter(mat);
        paint.setColorFilter(filter);
        c.drawBitmap(bmpOriginal, 0, 0, paint);
        bmpOriginal.recycle();
        return bmpGrayscale;
    }

    public static Bitmap changeBright(Bitmap bmpOriginal, int brightLevel) {
        Bitmap bmpChanged = Bitmap.createBitmap(bmpOriginal.getWidth(),
                bmpOriginal.getHeight(), bmpOriginal.getConfig());


        for (int i = 0; i < bmpOriginal.getWidth(); i++) {
            for (int j = 0; j < bmpOriginal.getHeight(); j++) {
                final int p = bmpOriginal.getPixel(i, j);
                final int r = Color.red(p) + brightLevel;
                final int g = Color.green(p) + brightLevel;
                final int b = Color.blue(p) + brightLevel;
                final int alpha = Color.alpha(p) + brightLevel;

                bmpChanged.setPixel(i, j, Color.argb(alpha, r, g, b));
            }
        }
        return bmpChanged;
    }

    public static Bitmap histogramEqualization(Bitmap bmpOriginal) {
        Bitmap bmpChanged = Bitmap.createBitmap(bmpOriginal.getWidth(),
                bmpOriginal.getHeight(), bmpOriginal.getConfig());
        double[] histogram = new double[GREYSCALE_HIGH_LEVEL];
        for (int i = 0; i < bmpOriginal.getWidth(); i++) {
            for (int j = 0; j < bmpOriginal.getHeight(); j++) {
                int histogramPosition = bmpOriginal.getPixel(i, j);
                histogram[histogramPosition]++;
            }
        }
        double pixelsCount = bmpOriginal.getWidth() * bmpOriginal.getHeight();
        for (int i = 0; i < GREYSCALE_HIGH_LEVEL; i++) {
            histogram[i] = histogram[i] / pixelsCount;
        }
        for (int i = 1; i < GREYSCALE_HIGH_LEVEL; i++) {
            histogram[i] = histogram[i - 1] + histogram[i];
        }
        for (int i = 0; i < bmpOriginal.getWidth(); i++) {
            for (int j = 0; j < bmpOriginal.getHeight(); j++) {
                bmpChanged.setPixel(i, j, (int) Math.round(histogram[bmpOriginal.getPixel(i, j)]));
                return bmpChanged;
            }
        }
        return bmpChanged;
    }
}
